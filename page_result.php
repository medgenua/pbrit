<?php

if (!isset($_GET['i']) || !file_exists("job_files/".$_GET['i']) ) {//|| !isset($_GET['j']) || !file_exists("job_files/".$_GET['i']."/".$_GET['j'])) {
	echo "<div class=section><h3>Invalid request</h3><p>Invalid job identifier provided.</p></div>";
	exit();
}


echo "<div style='margin-top:1em;'><a href='index.php?page=result&amp;i=".$_GET['i']."'>Results</a> ";
// no job_id or invalid job_id
if (!isset($_GET['j']) || !file_exists("job_files/".$_GET['i']."/".$_GET['j'])) {
	$job_dir = "job_files/".$_GET['i']."/";
	echo "</div><div class=section><h3>Available prioritization results</h3>";
	echo "<p><span class=emph>Note:</span> Results are kept on the server for 7 days</p>";
	$dirs = array_filter(glob("$job_dir*"), 'is_dir');
	if (count($dirs) == 0) {
		echo "<p>no results available.</p>";
	}
	else {
		echo "<ol>";
		foreach ($dirs as $dir) {
			$dir = str_replace("$job_dir","",$dir);
			$job_name = rtrim(file_get_contents($job_dir.$dir."/job_name"));
			echo "<li><a href='index.php?page=result&amp;i=".$_GET['i']."&amp;j=".$dir."'>$job_name</a>  <span class=italic>(Submitted at ".date('Y-m-d : H:i:s',$dir).")</span></li>";
		}
		echo "</ol>";
	}
	echo "</div>";
	exit;
}
else {
	$job_name = rtrim(file_get_contents("job_files/".$_GET['i']."/".$_GET['j']."/job_name"));
	echo " &#8702; <a href='index.php?page=result&amp;i=".$_GET['i']."&amp;j=".$_GET['j']."'>$job_name</a>";
	echo "</div>";
}

// get job_path
$ip_dir = $_GET['i'];
$job_dir = $_GET['j'];
$job_path = "job_files/$ip_dir/$job_dir";
// get job name
$job_name = rtrim(file_get_contents("$job_path/job_name"));
// get user email
$email = rtrim(file_get_contents("$job_path/email"));

// page output 
echo "<div class=section>";
echo "<h3>Prioritization Results : $job_name</h3>";

// first landing : print submission message 
if (isset($_GET['n']) && $_GET['n'] == 1) {
	echo "<p><span class=emph>Job submitted.</span> The results will be made available here. ";
	if ($email != '') {
		echo "You will also recieve an email directing you to this page when the results are available.";
	}
	echo "</p>";
}
// print autorefresh if status == unfinished.
$status =  rtrim(file_get_contents("$job_path/status"));
if ($status == 0) {
	echo "<p>This page autorefreshes every 30 seconds. </p>";
	echo "<meta http-equiv='refresh' content='30;URL=index.php?page=result&i=".$_SESSION['userip']."&j=".$job_dir."'>\n";
	echo "</div>";
	exit();
}

// print error message if status == -1
if ($status == -1) {
	// set permissions.
        $command = "COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && chmod 755 $job_path/job.e.txt $job_path/failed\"";
	system("$command");	
	echo "<p>Prioritization failed. The error message is shown below. Please report to the system admin.</p>";
	echo "<pre style='border:solid 1pt red'>";
	$error = file_get_contents("$job_path/failed");
	echo "$error\n";
	$error = file_get_contents("$job_path/job.e.txt");
	echo $error;
	echo "</pre>";
	echo "</div>";
	exit();
}

// show results if finished.
if ($status == 1) {
	// a hidden field with the path
	echo "<input type=hidden id='job_path' value='$job_path'/>";
	// release permissions
	echo "<p><span class=emph>Analysis Details:</span></p>";
	echo "<p><table cellspacing=0>";
	// method
	$method = rtrim(file_get_contents("$job_path/method"));
	
	echo "<tr><td class=bold NOWRAP>Analysis Method:</td><td>$method</td></tr>";
	// phenotype data used
	$up = array("1" => "Include Test Gene phenotype annotations in regression", "2" => "Exclude Test Gene phenotype annotations from regression");
	$use_pheno = $up[rtrim(file_get_contents("$job_path/use_pheno"))];
	echo "<tr><td class=bold NOWRAP>Test-Gene Annotation Handling:</td><td>$use_pheno</td></tr>";
	
	// Database Revision
	$db_version = rtrim(file_get_contents("$job_path/db_version"));
	echo "<tr><td class=bold NOWRAP>Annotation Data Revision:</td><td><input type=hidden id='db_version' value='$db_version'/>$db_version</td></tr>";

	// Training Genes (n)
	$train_genes = explode("\n",file_get_contents("$job_path/train_genes.txt"));
	echo "<tr><td class=bold NOWRAP>Training Genes (n=".count($train_genes)."):</td><td> ".implode(", ",$train_genes)."</td></tr>";

	// Excluded Training Genes
	$bad_train_genes = array();
	if (file_exists("$job_path/bad_train_genes.txt")) {
		$bad_train_genes =  explode("\n",rtrim(file_get_contents("$job_path/bad_train_genes.txt")));
		echo "<tr><td class=bold NOWRAP style='vertical-align:top;'>Excluded Training Genes</td><td>".implode(", ",$bad_train_genes)."</td></tr>";
	}
	// Excluded Test Genes?
	if (file_exists("$job_path/bad_test_genes.txt")) {
		$bad_test_genes =  explode("\n",rtrim(file_get_contents("$job_path/bad_test_genes.txt")));
		echo "<tr><td class=bold NOWRAP style='vertical-align:top;'>Excluded Test Genes</td><td>".implode(", ",$bad_test_genes)."</td></tr>";
		
	}
	// runtime
	$query = mysql_query("SELECT `run_time` FROM `Submitted_Jobs` WHERE `job_id`  = '$job_dir'");
	$row = mysql_fetch_array($query);
	$runtime = floor($row[0]/3600) . "h:".floor($row[0] / 60 % 60).'m:'. floor($row[0] % 60) .'s';
	echo "<tr><td class=bold NOWRAP style='vertical-align:top;'>Run-Time</td><td>$runtime</td></tr>";
	echo "</table></p>";
	echo "<p><span class=emph>Ranking Output</span></p>";
	echo "<p><b>LEFT: </b><span>Prioritization Result: <b>RANK</b>: Rank of Gene; <b>RANK-RATIO</b>: Ratio of Rank and total number of candidate genes; <b>GENE</b>: Hugo Symbol of Genes; <b>SCORE</b>: Predicted concordance score after regression; <b>EXPLORE</b>: For each gene, the user can explore relative contribution of each annotation sources with respect to the training genes for its overall rank.</span></p>";
	echo "<p><b>RIGHT: </b><span><b>Global Similarity Map</b>: The heatmap plot of funcional annotation matrix X used to predict the phenotype concordance scores. The X-axis denotes the training genes and Y-axis denotes combined set of Training (BLUE) and Test(RED) genes. The Test genes are sorted according to their rank. </span><b>Explore Ranking Details</b>: tabular overview of individual features and the corresponding tf-idf scores. <b>Regression Details</b>: Plot of beta values after training. Link to download the full regression model as an R-object.</p>";
	
	// rank table
	echo "<div style='width:47%;margin-right:3%;float:left;position:relative;'>";
	echo "<p><table width='100%' cellspacing=0>";
	echo "<tr><th>Rank</th><th>Rank-Ratio</th><th>Gene</th><th> Score</th><th>Explore</th></tr>";
	$nrlines=rtrim(`cat '$job_path/output_hgnc_list.txt' | wc -l`);
	$fh = fopen("$job_path/output_hgnc_list.txt",'r');
	while (($line = fgets($fh)) !== false) {
		$c = explode("\t",str_replace('"','',rtrim($line)));
		echo "<tr><td>$c[0]</td><td>".round(100*$c[0]/$nrlines,2)."%</td><td>$c[1]</td><td>".round($c[2],5)."</td><td><img src='images/content/explore2.png' style='height:1.5em;margin-top:-0.3em;cursor:pointer;' onClick=\"LoadHeatMap('$c[1]','$job_path')\" /></td></tr>";
	}
	echo "<tr><td colspan=4 class=last>&nbsp;</td></tr>";
	echo "</table></p></div>";
	fclose($fh);
	// right side toggle switches.
	echo "<div style='width:49%' class=toright>";
	echo "<p><table width='100%' cellspacing=0>";
	echo "<tr><th onclick=\"toggle_display('heatmap')\" onmouseover='' style='cursor: pointer;' >Global Similarity Map</th><th onclick=\"toggle_display('ranking_details')\" onmouseover='' style='cursor: pointer;'  >Explore Ranking Details</th><th onclick=\"toggle_display('regression_details')\" onmouseover='' style='cursor: pointer;' >Regression Details</th></tr>";
	echo "</table>";
	// global heatmap
	echo "<div id='heatmap'>";
	if (file_exists("$job_path/ranking_heatmap.png")) {
		echo "<p><span class='italic underline'>Global Gene_to_Gene similarity heatmap:</span></p>";
		echo "<a href='$job_path/ranking_heatmap.png' rel=lightbox><img src='$job_path/ranking_heatmap.png' style='width:98%' rel=lightbox title='click to enlarge'/></a>";
	}
	else {
		echo "<p>Heatmap not available.</p>";
	}
	echo "</div>";


	// ranking details browser.
	echo "<div id='ranking_details' style='display:none'>";
	echo "<p><span class='italic underline'>Make a selection to explore individual annotation features contributing to the ranking:</span><br/><br/>&nbsp; Features are ranked in descending TF-IDF score for the test-gene. <br/>&nbsp; Corresponding scores for the training genes are listed if available.</p>";
	echo "<p><table >";
	echo "<tr><td>Test gene:</td><td><select id='browser_testgene' onchange='LoadRankDetails()'>";
	echo "<option value='' disabled selected>--</option>";
	// symbol->ensG list exists?
	if (!file_exists("$job_path/test_mapping.txt")) {
		// READ GENE MAP
		$query = mysql_query("SELECT `HGNC_NAME`,`ENSG_ID` FROM `$db_version"."_HGNC_ENSG_MAP`") ;
		$h_2_e = array();
		$list = array();
		//$e_2_h = array();
		while ($row = mysql_fetch_array($query,MYSQL_ASSOC)) {
			$h_2_e[$row['HGNC_NAME']] = $row['ENSG_ID'];
			//$e_2_h[$row['ENSG_ID']] = $row['HGNC_NAME'];
		}

		$fh = fopen("$job_path/test_genes.txt",'r');
		$fo = fopen("$job_path/test_mapping.txt","w");
		while (($line = fgets($fh)) !== false) {
			$line = rtrim($line);	
			if (array_key_exists($line,$h_2_e)) {
				//fwrite($fo,"$line\t".$h_2_e[$line]."\n");
				$list[$line] = $h_2_e[$line];
			}
		}
		ksort($list);
		foreach($list as $hugo => $ens) {
			fwrite($fo,"$hugo\t$ens\n");
		}
		fclose($fh);
		fclose($fo);
	}
	$fh = fopen("$job_path/test_mapping.txt",'r');
	while (($line = fgets($fh)) !== false) {
		$line = rtrim($line);
		list($hugo,$ens) = explode("\t",$line);
		echo "<option value='$ens'>$hugo</option>";
	}
	fclose($fh);
	echo "</select> </td></tr> ";
	echo "<tr><td>Training gene:</td><td><select id='browser_traingene'  onchange='LoadRankDetails()'>";
	echo "<option value='' disabled selected>--</option>";
	// symbol->ensG list exists?
	if (!file_exists("$job_path/train_mapping.txt")) {
		// READ GENE MAP
		$query = mysql_query("SELECT `HGNC_NAME`,`ENSG_ID` FROM `$db_version"."_HGNC_ENSG_MAP`") ;
		$h_2_e = array();
		$list = array();
		//$e_2_h = array();
		while ($row = mysql_fetch_array($query,MYSQL_ASSOC)) {
			$h_2_e[$row['HGNC_NAME']] = $row['ENSG_ID'];
		}

		$fh = fopen("$job_path/train_genes.txt",'r');
		$fo = fopen("$job_path/train_mapping.txt","w");
		while (($line = fgets($fh)) !== false) {
			$line = rtrim($line);	
			if (array_key_exists($line,$h_2_e)) {
				//fwrite($fo,"$line\t".$h_2_e[$line]."\n");
				$list[$line] = $h_2_e[$line];
			}
		}
		ksort($list);
		foreach($list as $hugo => $ens) {
			fwrite($fo,"$hugo\t$ens\n");
		}
		fclose($fh);
		fclose($fo);
	}
	$fh = fopen("$job_path/train_mapping.txt",'r');
	while (($line = fgets($fh)) !== false) {
		$line = rtrim($line);
		list($hugo,$ens) = explode("\t",$line);
		echo "<option value='$ens'>$hugo</option>";
	}
	fclose($fh);
	echo "</select></td></tr> ";
	echo "<tr><td>Annotation Source:</td><td><select id='browser_anno'  onchange='LoadRankDetails()'>";
	echo "<option value='' disabled selected>--</option>";
	echo "<option value=BL>Protein Blast</option>";
	echo "<option value=DP>Disease Ontology</option>";
	echo "<option value=GD>Gene Association Database</option>";
	echo "<option value=GO>Gene Ontology</option>";
	echo "<option value=HD>HuGe Disease Navigator</option>";
	echo "<option value=HP>Human Phenotype Ontology</option>";
	echo "<option value=MP>Mammalian Phenotype Ontology</option>";
	echo "<option value=PB>Pubmed Abstracts</option>";
	echo "<option value=PP>PPI (ConsensusPathDB)</option>";
	echo "<option value=PY>Pathways (ConsensusPathDB)</option>";
	echo "</select></td></tr>";
	echo "</table></p>";
	echo "<div id='rank_browser_table'></div>";

	echo "</div>";


	// regression details.
	echo "<div id='regression_details' style='display:none'></div>";
	if (file_exists("$job_path/pBRIT_fm_object.dat")) {
		/*	
		if (!file_exists("$job_path/model_betas.png")) {
			system("/opt/software/R/3.2.1/bin/Rscript '$scriptdir/Analysis_Files/parse_pBRIT.object.Rscript' '$job_path/'");
		}

		if (!file_exists("$job_path/model_betas.png")) {
			echo "<p>ERROR: Beta-plot could not be created</p>\n";
		}
		else {
			echo "<p><img src='$job_path/model_betas.png' style='width:100%' /></p>";
		}
		echo "<p>Download Full pBRIT Regression model: <a href='pBRIT_fm_object.dat'>Right-Click and Save-As</a></p>";
		*/
	}
	else {
		echo "<p>Datafile for regression model not found. Details are not available.</p>";
	}
	echo "<script type='text/javascript'>LoadModelDetails('$job_path')</script>";
	echo "</div>";
	echo "<br style='clear:both;'/>";
}


?>
