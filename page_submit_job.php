<?php
// this is a fake field to trick bots.
if ($_POST['email'] != 'Provide your email') {
	echo "thank you for contacting us.";
	$fh = fopen("bot_posts.txt",'a');
	fwrite(date('Y-m-d'). " ip:".get_user_ip()."\n");
	fclose($fh);
	exit;
}

// email
if (isset($_POST['notify']) && filter_var($_POST['notify'], FILTER_VALIDATE_EMAIL)) {
	$email = $_POST['notify'];
}
else {
	$email = '';
}

// job name
if (isset($_POST['jobname']) && $_POST['jobname'] != '') {
	$job_name = addslashes($_POST['jobname']);
}
else {
	$job_name = date('Y-m-d : H:i:s');
}




// method
if (isset($_POST['method'])) {
	$method = $_POST['method'];
}
else {
	echo "<div class=section>";
	echo "<h3>Job Submission : $job_name</h3>";
	echo "<p>Error : No analysis method provided </p><p><button onclick='goBack()'>Go Back</button></p></div>";
	exit;

}
// db-release
if (isset($_POST['db_version'])) {
	$db_version = $_POST['db_version'];
}
else {
	$db_version = 'Jan2015';	
}
// use_pheno
if (isset($_POST['use_pheno'])) {
	$use_pheno = $_POST['use_pheno'];
}
else {
	$use_pheno = '1';	
}


// READ GENE MAP
$query = mysql_query("SELECT `HGNC_NAME`,`ENSG_ID` FROM `$db_version"."_HGNC_ENSG_MAP`") ;
$h_2_e = array();
$e_2_h = array();
while ($row = mysql_fetch_array($query,MYSQL_ASSOC)) {
	$h_2_e[$row['HGNC_NAME']] = $row['ENSG_ID'];
	$e_2_h[$row['ENSG_ID']] = $row['HGNC_NAME'];
}
// training genes
$train_good = array();
$train_bad = array();
$test_good = array();
$test_bad = array();
if (isset($_POST['trainingset'])) {
	$entries = split("\n",$_POST['trainingset']);
	foreach ($entries as $entry) {
		$entry = rtrim($entry);
		$entry = str_replace(" ","",$entry);
		$entry = str_replace(",","",$entry);
		if ($entry == '') {
			continue;
		}
		if (in_array($entry,$train_good) && !in_array("$entry (double entry)",$train_bad)) {
			array_push($train_bad,"$entry (double entry)");
			continue;
		}
		// hugo?
		if (array_key_exists($entry,$h_2_e)) {
			array_push($train_good,$entry);
		}
		// ensembl ?
		elseif (array_key_exists($entry,$e_2_h)) {
			array_push($train_good,$e_2_h[$entry]);
		}
		// region?
		elseif (preg_match("/^(chr)*([\dXYM]+):(\d+)-(\d+)$/",$entry,$matches)) {
			// get genes in provided region, using build hg19. TODO
			array_push($train_bad,"$entry (regions are not supported yet)");
		}
		// not recognized
		else {
			array_push($train_bad,"$entry (symbol not recognized in internal database)");
		}
	}
	
}
if (isset($_POST['testset'])) {
	$entries = split("\n",$_POST['testset']);
	foreach ($entries as $entry) {
		$entry = rtrim($entry);
		$entry = str_replace(" ","",$entry);
		$entry = str_replace(",","",$entry);
		if ($entry == '') {
			continue;
		}
		if (in_array($entry,$test_good) && !in_array("$entry (double entry)",$test_bad)) {
			array_push($test_bad,"$entry (double entry)");
			continue;
		}
		if (in_array($entry,$train_good)) {
			array_push($test_bad,"$entry (training gene)");
			continue;
		}

		// hugo?
		if (array_key_exists($entry,$h_2_e)) {
			array_push($test_good,$entry);
		}
		// ensembl ?
		elseif (array_key_exists($entry,$e_2_h)) {
			array_push($test_good,$e_2_h[$entry]);
		}
		// region?
		elseif (preg_match("/^(chr)*([\dXYM]+):(\d+)-(\d+)$/",$entry,$matches)) {
			// get genes in provided region, using build hg19. TODO
			array_push($test_bad,"$entry (regions are not supported yet)");
		}
		// not recognized
		else {
			array_push($test_bad,"$entry (symbol not recognized in internal database)");
		}
	}
}
// exit if either no valid test/train genes
if (count($train_good) == 0) {
	echo "<div class=section>";
	echo "<h3>Job Submission : $job_name</h3>";
	echo "<p>Error : No training genes provided or recognized</p><p><button onclick='goBack()'>Go Back</button></p></div>";
	exit;
}

if (count($test_good) == 0) {
	echo "<div class=section>";
	echo "<h3>Job Submission : $job_name</h3>";
	echo "<p>Error : No test genes provided or recognized</p><p><button onclick='goBack()'>Go Back</button></p></div>";
	exit;
}


// create input files.
$job_id = time();
$job_dir = "job_files/".$_SESSION['userip']."/".$job_id;
while (file_exists($job_dir)) {
	$job_id = time();
    	$job_dir = "job_files/".$_SESSION['userip']."/$job_id";
}

// user dir, based on hashed ip.
mkdir($job_dir,0777,true);
$result_path = $job_dir;
$job_dir = realpath($job_dir);
$job_contents = file_get_contents("Analysis_Files/job_template.txt");
$job_contents = str_replace('<job_ip>',$_SESSION['userip'],$job_contents);
$job_contents = str_replace('<job_dir>',$job_dir,$job_contents);
$job_contents = str_replace('<job_id>',$job_id,$job_contents);
$job_contents = str_replace('<method>',$method,$job_contents);
$job_contents = str_replace('<use_pheno>',$use_pheno,$job_contents);
$job_contents = str_replace('<db_version>',$db_version,$job_contents);
$job_contents = str_replace('<script_dir>',$config['SCRIPTDIR'],$job_contents);
#activate beta code
if (isset($_POST['beta']) && $_POST['beta'] == 1) {
	# remove comment in front of beta commands
	$job_contents = str_replace('#beta#','',$job_contents);
	# replace R script by the beta version.
	$job_contents = str_replace('pBRIT.R','pBRIT.beta.R',$job_contents);
}
$fh = fopen("$job_dir/job.sh",'w');
fwrite($fh,$job_contents);
fclose($fh);
// write email to job_dir
$fh = fopen("$job_dir/email","w");
fwrite($fh,"$email");
fclose($fh);
// create status file
$fh = fopen("$job_dir/status","w");
fwrite($fh,"0");
fclose($fh);
// write job name.
$fh = fopen("$job_dir/job_name","w");
fwrite($fh,$job_name);
fclose($fh);
// write use_pheno flag.
$fh = fopen("$job_dir/use_pheno","w");
fwrite($fh,$use_pheno);
fclose($fh);
// write db_version .
$fh = fopen("$job_dir/db_version","w");
fwrite($fh,$db_version);
fclose($fh);
//write method
$fh = fopen("$job_dir/method","w");
fwrite($fh,$method);
fclose($fh);

// write training genes
$fh = fopen("$job_dir/train_genes.txt","w");
fwrite($fh,implode("\n",$train_good));
fclose($fh);
// print excluded training genes
if (count($train_bad) > 0) {
	$fh = fopen("$job_dir/bad_train_genes.txt","w");
	foreach($train_bad as $entry) {
		fwrite($fh,"$entry\n");
	}
	fclose($fh);
}

// write training genes
$fh = fopen("$job_dir/test_genes.txt","w");
fwrite($fh,implode("\n",$test_good));
fclose($fh);

// print excluded test genes
if (count($test_bad) > 0) {
	$fh = fopen("$job_dir/bad_test_genes.txt","w");
	foreach($test_bad as $entry) {
		fwrite($fh,"$entry\n");
	}
	fclose($fh);
}

# make subdir for data_files
system("mkdir -p '$job_dir/data_files'");
# make writable.
system("chmod -R 777 $job_dir");
$fh = fopen("job_queue/web_queue",'a');
if (flock($fh, LOCK_EX)) {
 	fwrite($fh,"$job_dir\n");
	flock($fh, LOCK_UN); // unlock the file
	fclose($fh);
	echo "<meta http-equiv='refresh' content='0;URL=index.php?page=result&amp;i=".$_SESSION['userip']."&amp;j=".$job_id."&amp;n=1'>\n";

	
} else {
	// flock() returned false, no lock obtained
	echo "<div class=section>";
	echo "<h3>Job Submission : $job_name</h3>";
	echo "ERROR: Could not lock queue file. Job was NOT submitted!\n";
	exit();
}





?>
