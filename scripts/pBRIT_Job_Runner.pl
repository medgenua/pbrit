#!/usr/bin/env perl
$|++;
## RUN AS SCRIPTUSER

## modules
use Cwd 'abs_path';

## credentials
$credpath = abs_path("../includes/LoadCredentials.pl");
require($credpath);

my %track_status = ();
my %emails = ();
my $mail = 0;
## inifite loop
while ( 1 ) {
	# nr to be queued
	my @web_queue = `cat ../job_queue/web_queue`;
	chomp(@web_queue);
	my @api_queue = `cat ../job_queue/api_queue`;
	chomp(@api_queue);
	if (scalar(@web_queue) + scalar(@api_queue) == 0) {
		my $date = `date`;
		chomp($date);
		print "$date : nothing to do.\n";
		goto TRACK;
	}
	# nr on queue
	my $nr = `qselect -u $scriptuser -s RQ | wc -l`;
	chomp($nr);
	
	my $job;
	# web-queue (re-read to minimize race conditions)
	my @web_queue = `cat ../job_queue/web_queue`;
	open WQ, ">../job_queue/web_queue";
	flock(WQ,2);	
	chomp(@web_queue);
	while ($nr < $max_on_queue && defined($job_dir = shift @web_queue )) {
		my $date = `date`;
		chomp($date);
		print "$date : Submitting web '$job_dir/job.sh'.\n";


		my $job_id = `qsub $job_dir/job.sh`;
		chomp($job_id);
		my $try = 0;
		while ($job_id !~ m/^\d+\./ && $try < 10) {
			sleep 5;
			$job_id = `qsub $job_dir/job.sh`;
			$try++;
		}
		# submission failed.
		if ($job_id !~ m/^\d+\./) {
			open OUT, ">$job_dir/status";
			print OUT "-1";
			close OUT;
			open OUT, ">$job_dir/failed";
			print OUT "Could not submit job to HPC";
			close OUT;
			next;
		}
		open OUT, ">$job_dir/hpc_id";
		print OUT $job_id;
		close OUT;
		$track_status{$job_dir} = 0;
		$nr++;

	}
	print WD join("\n",@web_queue);
	close(WQ);

	# api-queue
	my @api_queue = `cat ../job_queue/api_queue`;
	open AQ, ">../job_queue/api_queue";
	flock(AQ,2);	
	chomp(@api_queue);
	while ($nr < $max_on_queue && defined($job_dir = shift @api_queue ) ) {
		my $date = `date`;
		chomp($date);
		print "$date : Submitting api '$job_dir/job.sh'.\n";

		my $job_id = `qsub $job_dir/job.sh`;
		chomp($job_id);
		my $try = 0;
		while ($job_id !~ m/^\d+\./ && $try < 10) {
			sleep 5;
			$job_id = `qsub $job_dir/job.sh`;
			$try++;
		}
		# submission failed.
		if ($job_id !~ m/^\d+\./) {
			open OUT, ">$job_dir/status";
			print OUT "-1";
			close OUT;
			open OUT, ">$job_dir/failed";
			print OUT "Could not submit job to HPC";
			close OUT;
			next;
		}
		open OUT, ">$job_dir/hpc_id";
		print OUT $job_id;
		close OUT;
		$track_status{$job_dir} = 0;
		$nr++;
	}
	print AQ join("\n",@api_queue);
	close AQ;	
	TRACK:
	## update STATUS
	foreach my $job_dir (keys(%track_status)) {
		my $status = `cat $job_dir/status`;
		chomp($status);
		# finished
		if ($status == 1) {
			my $email = `cat $job_dir/email`;
			chomp($email);
			my $job_name = `cat $job_dir/job_name`;
			chomp($job_name);
			if ($email ne '') {
				$emails{$email}{'success'}{$job_dir} = $job_name;
			}
			delete($track_status{$job_dir});
		}
		# failed
		if ($status == -1) {
		 	my $email = `cat $job_dir/email`;
			chomp($email);
			if ($email ne '') {
				my $reason = `cat $job_dir/failed`;
				chomp($reason);
				$emails{$email}{'failed'}{$job_dir}{'name'} = $job_name;
				$emails{$email}{'failed'}{$job_dir}{'reason'} = $reason;
			}
			delete($track_status{$job_dir});
		}
	}
	$mail++;
	if ($mail == 10) {
		$mail = 0;
		# send emails.
		foreach my $email (keys(%emails)) {
			my $tmpfile = `mktemp`;
			chomp($tmpfile);
			open OUT, ">$tmpfile";
			print OUT "To: $email\n";
			print OUT "BBC: geert.vandeweyer\@uantwerpen.be\n";
			print OUT "From: no-reply\@biomina.be\n";
			print OUT "Subject : pBRIT Jobs finished\n";
			print OUT "Dear User,\r\n\r\n";
			if (scalar(keys(%{$emails{$email}{'success'}})) >0){
				print OUT "The following jobs are finshed. Results are available at the provided links: \r\n";
				foreach my $job_dir (keys(%{$emails{$email}{'success'}})) {
					my @p = split(/\//,$job_dir);
					my $url = "$web_location/index.php?page=result&i=$p[-2]&j=$p[-1]";
					print OUT "  - ".$emails{$email}{'success'}{$job_dir}." : $url\r\n";
				}
				print "\r\n";
				
			}
			if (scalar(keys(%{$emails{$email}{'failed'}})) >0){
				print OUT "The following jobs failed. An indication of the reason is given: \r\n";
				foreach my $job_dir (keys(%{$emails{$email}{'failed'}})) {
					print OUT "  - ".$emails{$email}{'failed'}{$job_dir}{'name'}." : ".$emails{$email}{'failed'}{$job_dir}{'reason'}."\r\n";
				}
				print "\r\n";
				
			}
			close OUT;
			my $date = `date`;
			chomp($date);
			print "$date : sending mail to $email\n"; 
			system("sendmail -t < $tmpfile");
			system("rm $tmpfile");
			delete($emails{$email});

		}

	}

	sleep 30;
}
