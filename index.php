<?php
session_start();
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href='http://143.169.238.105/pbrit/' />
<title>pBRIT</title>
<meta name="description" content="pBRIT : Gene prioritization tool" />
<meta name="keywords" content="gene prioritization bayesian ridge regression" />
<link rel='stylesheet' type='text/css' href='stylesheets/All_Format.css'/>
<!--<script type="text/javascript" src="javascripts/menu2.js"></script>-->
<script type="text/javascript" src="javascripts/javascripts.js"></script>
<script type='text/javascript' src='javascripts/lightbox.js'></script>
<link rel='stylesheet' type='text/css' href='stylesheets/lightbox.css' />
<script type="text/javascript" async   src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_SVG">
</script>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
</head>
<body>
<div id='overlay' style='display:none;'><!-- general purpose overlay div --></div>
<div id='mapoverlay' style='display:none;'><!-- heatmap overlay div for page_results --></div>

<?php
#############################
## CONNECT TO THE DATABASE ##
#############################
include('includes/LoadCredentials.php');
include('includes/functions.php');
if (!isset($_SESSION['userip'])) {
	$_SESSION['userip'] = md5(get_user_ip());
}
# set page
if (isset($_GET['page'])) {
	$page = "page_" . $_GET['page'] .'.php';
}
else {
	$page = "page_main.php";
	#$page = "pBRIT_Intro_test.php";
}
?>
<div id=container style='position:relative;width:1080px;margin-left:auto;margin-right:auto;text-align:left'>
<div id="content-head">
	<div id='content-headleft'>
		<h1>pBRIT  
		<span style=';font-style=italic;padding-right:5px;font-size:12px'>candiate gene prioritization</span>
		</h1>
	</div>
 	<div id="content-headright"><img src='images/layout/CMG_logo-en-naam_RGB.png' style='height:3.5em;margin-right:0.75em'></div>
</div>
<!-- main contents -->

<div id='content-left' >
		<ul id=topmenu>
		<li><a href='index.php?page=main'>Main</a></li>
		<li> | <a href='index.php?page=run'>Prioritize</a></li> 
		<li> | <a href='index.php?page=run_example'>Example Data</a></li>
		<?php
		if (isset($_SESSION['userip']) && file_exists("job_files/".$_SESSION['userip'])) {
			echo "<li> | <a href='index.php?page=result&amp;i=".$_SESSION['userip']."'>Results</a></li>";
		}
		?>
		<li> | <a href='index.php?page=status'>Queue Status</a></li>
		<li> | <a href='index.php?page=validation'>Cross-Validation Results</a></li>
		<li> | <a href='index.php?page=downloads'>Downloads</a></li>
		<li> | <a href='index.php?page=documentation'>Documentation</a></li>
		</ul>
		<span id=CreatedSpan><a id=CreatedBy style='font-style:italic;margin:0;padding:0;background-color:#efefef;font-weight:normal;text-decoration:none;letter-spacing:normal' href='index.php?page=contact'>contact</a></span>
</div> 

 <div id='content-right' >
	<?php

	if (file_exists("$page")) {
		include("$page");
	}
	else {
		include("page_main.php");
	}
	?>
</div>

</div> <!-- end of container -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77124224-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
	d = document.getElementById("mapoverlay");
    	addEventListener("keydown",    function(e){e.keyCode==27 &&(d.style.display="none")},false);
</script>

</body>
</html>
