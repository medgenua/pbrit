<?php

// hash the path to get usable string
if (isset($_GET['p'])) {
	$path_items = explode("/",stripslashes($_GET['p']));
}
$p_string = '';
echo "<div style='margin-top:1em;font-size:0.9em'><a class=italic href='index.php?page=validation'>Validation Data</a>";
$last = '';
foreach($path_items as $item) {
	if ($item == '') {
		continue;
	}
	$p_string .= "$item/";
	echo " &#8702; <a class=italic href='index.php?page=validation&p=".addslashes(substr($p_string,0,-1))."'>$item</a>";
	$last = $item;
}
echo "</div>";


$depth = count($path_items);

$umls = array();
// top level: short general intro.
if ($depth == 0) {
	echo "<div class=section>";
	echo "<h3>Validation Results</h3>";
	echo "<p>Full validation results are presented for reviewing, ordered as:  'DataSet' &gt; 'pBRIT-Validation-scenario' &gt; 'Disease/Class' &gt; 'Inputs &amp; Results'.</p>";
}
// load umls codes.
elseif ($depth >= 3) {
	$fh = fopen("Validation_Data/umls.map.txt",'r');
	while (($line = fgets($fh)) !== false) {
		$line = rtrim($line);
		$p = explode("\t",$line);
		$p[0] = str_replace(" ","",$p[0]);
		$umls[$p[0]] = $p[1];
	}
	fclose($fh);
}
echo "<div class=section>";
if (array_key_exists($item,$umls)) {
	$item .= " (".$umls[$item].')';
}
echo "<h3>Validation Results: $item</h3>";

// print parent level summary.
$level_info = array('','Validation Source','Validation Scenario','Disease/Class');
if ($depth > 0) {
	echo "<p><span class=emph>Validation Strategy:</span><table style='font-size:1.1em;margin-left:0.5em'>";
	for ($i = 1 ; $i <= $depth ; $i++) {
		echo "<tr><td style='padding:0;'><span class=bold>".$level_info[$i].":</span></td><td style='padding:0;'>".$path_items[$i-1]."</td></tr>";
	}
	echo "</table></p>";
}

// browsing.
if ($depth < 3) {
	echo "<p><span class=emph>Select subfolder:</span><ul class=disc>";
	if (file_exists("Validation_Data/$p_string/.dir_contents") ) {
		$dirs = explode("\n",file_get_contents("Validation_Data/$p_string/.dir_contents"));
	}
	else {
		$dirs = array_filter(glob("Validation_Data/$p_string*"), 'is_dir');
		$fh = fopen("Validation_Data/$p_string/.dir_contents",'w');
		fwrite($fh,implode("\n",$dirs));
		fclose($fh);
	}
	foreach($dirs as $dir) {
		$d_parts = explode("/",$dir);
		array_shift($d_parts);
		$item = end($d_parts);
		if (array_key_exists($item,$umls)) {
			$item .= " : ".$umls[$item];
		}
		echo "<li><a href='index.php?page=validation&p=".addslashes(implode("/",$d_parts))."'>$item</a></li>";
	
	}
	echo "</ul></p>";
}
// overview of the LOOCV
else {
	if (!preg_match("/4.Literature.Genes/",$p_string)) {
		echo "<p><span class=emph>Leave-One-Out-Cross-Validation Results</span></p>"; 
		$train_genes = explode("\n",rtrim(file_get_contents("Validation_Data/$p_string/$last"."_Train.txt")));
		// list training genes
		echo "<p><span class='italic underline'>Training Genes: </span><span class=italic> (n = ".count($train_genes).")</span></p><p><span style='margin-left:1em;'>".implode(", ",$train_genes)."</span></p>";
		// list ranks of training genes.
		echo "<div style='position:relative;width:45%;float:left;margin-right:3%'>";
		echo "<p><span class='italic underline'>Obtained Ranks: </span></p><p><table style='margin-left:1em;;width:100%' cellspacing=0>";
		echo "<tr><th>Gene Symbol</th><th>Rank</th><th>Score</th><th>Rank Ratio</th><th>SSE</th></tr>";
		if (file_exists("Validation_Data/$p_string/RANK/Ranking_gene_list_11.txt")) {
			$fh = fopen("Validation_Data/$p_string/RANK/Ranking_gene_list_11.txt",'r');
		}
		elseif (file_exists("Validation_Data/$p_string/RANK/$last"."_Ranking_gene_list_11.txt")) {
			$fh = fopen("Validation_Data/$p_string/RANK/$last"."_Ranking_gene_list_11.txt",'r');
		}
		$colidxs = array(0,1,2,4,9);
		while (($line = fgets($fh)) !== false) {
			$cols = explode("\t",rtrim($line));
			$out = "<tr>";
			foreach ($colidxs as $i) {
				$cols[$i] = str_replace(" ","",$cols[$i]);
				if ($cols[$i] == '') {
					$cols[$i] = '&lt;NA&gt;';
				}
				$out .= "<td>".$cols[$i]."</td>";
			}
			echo "$out</tr>";
		}
		echo "<tr><td colspan=10 class=last>&nbsp;</td></tr></table></p></div>";
		// show the ROC plot.
		if (!file_exists("Validation_Data/$p_string/roc.png")) {
			system("/opt/software/R/3.2.1/bin/Rscript /pBRIT/Analysis_Files/getTPR.R 'Validation_Data/$p_string/RANK/Ranking_gene_list_11.txt' 'Validation_Data/$p_string/roc.png' '$last' >/dev/null 2>&1");
		}
		echo "<div style='position:relative;with:45%;float:left;margin-right:3%'>";
		echo "<p><span class='italic underline'>LOOC ROC plot: </span></p>";
		echo "<p><img style='width:100%' src='Validation_Data/$p_string/roc.png' /></p>";
		echo "</div>";
	
		
		echo "<br style='clear:both;'/>";
		// show selector for LOOCV iteration details.
		echo "<p><span class='italic underline'>LOOCV iterations: </span><select id='loocv_path' name='loocv_path'>";
		$selected = 'selected=selected';
		for ($i = 1;$i<=count($train_genes);$i++) {
			$path = "Validation_Data/$p_string/REPLACE_THIS_$i.txt";
			echo "<option value='$path' $selected>$i : ".$train_genes[$i-1]."</option>";
			$selected = '';
		}
		echo "</select> <button onClick='load_loocv()'  id=LoadDetails >Show Details</button><p>";
		echo "<div id=loocv_results></div>";

	}
	else {
		echo "<p><span class=emph>Literature Benchmark Validation Results</span></p>"; 
		$train_genes = explode("\n",rtrim(file_get_contents("Validation_Data/$p_string/$last"."_Train.txt")));
		// list training genes
		echo "<p><span class='italic underline'>Training Genes: </span><span class=italic> (n = ".count($train_genes).")</span></p><p style='margin-left:1em;'><span >".implode(", ",$train_genes)."</span></p>";
		$test_genes = explode("\n",rtrim(file_get_contents("Validation_Data/$p_string/TEST/Test_1.txt")));
		// list training genes
		echo "<p><span class='italic underline'>Training Genes: </span><span class=italic> (n = ".count($train_genes).")</span></p><p style='margin-left:1em;'><span >".implode(", ",$test_genes)."</span></p>";

		// get details.
		$seed = "Validation_Data/$p_string/SEED/Seed_1.txt";
		$train = "Validation_Data/$p_string/TRAIN/Train_1.txt";
		$test = "Validation_Data/$p_string/TEST/Test_1.txt";
		echo "<p><span class='italic underline'>Ranking Output: </span></p>";

		// rank table
		echo "<span style='width:47%;margin-right:3%;float:left;position:relative;'>";
		echo "<table width='100%' cellspacing=0 style='margin-top:1em;'>";
		echo "<tr><th>Rank</th><th>Rank-Ratio</th><th>Gene</th><th>Score</th><th>Explore</th></tr>";
		$path = "Validation_Data/$p_string";
		$comm = "ls $path/OUTPUT/$last"."_output*txt";
		$outfile = rtrim(`$comm`);
		$nrlines=rtrim(`cat '$outfile' | wc -l`);
		$fh = fopen("$outfile",'r');
		$method = 'TFIDF';
		if (preg_match("/TFIDF_SVD/",$path)) {
			$method = 'SVD';
		}

		while (($line = fgets($fh)) !== false) {
			$c = explode("\t",str_replace('"','',rtrim($line)));
			echo "<tr><td>$c[0]</td><td>".round(100*$c[0]/$nrlines,2)."%</td><td>$c[1]</td><td>".round($c[2],5)."</td><td><img src='images/content/explore2.png' style='height:1.5em;margin-top:-0.3em;cursor:pointer;' onClick=\"LoadValidationHeatMap('$c[1]','$path','$method','Jan2015','$last')\" /></td></tr>";
		}
		echo "<tr><td colspan=4 class=last>&nbsp;</td></tr>";
		echo "</table></span></li>";
		fclose($fh);
		// heatmap.
		$comm = "ls $path/IMAGES/$last"."_*png";
		$outfile = rtrim(`$comm`);
		if ($outfile != '') {
			echo "<div class='toright' style='width:49%'><img src='$outfile' style='width:100%'/></div>";
		}
		
		echo "<br style='clear:both;'/>";

		
	}

	
}

echo "</div>";

