<?php
echo "<div class=section>";
echo " <h3>Queue Status</h3>";

// unsubmitted web_queue 
$web_queue = intval(exec("wc -l 'job_queue/web_queue'"));

// unsubmitted api_queue 
$api_queue = intval(exec("wc -l 'job_queue/api_queue'"));

// cluster queued
$cluster_queued =rtrim(shell_exec("qstat -u $scriptuser -i"));
if ($cluster_queued == '') {
	$cluster_queued = 0;
}
else {
	$cluster_queued = count(explode("\n",$cluster_queued)) - 5;
}
// cluster running
$cluster_running = rtrim(shell_exec("qstat -u $scriptuser -r"));
if ($cluster_running == '') {
	$cluster_running = 0;
}
else {
	$cluster_running = count(explode("\n",$cluster_running)) - 5;
}

echo "<p><table cellspacing=0 width='100%'>";
echo "<tr>";
echo "<th>Running</th><th>Scheduled for running</th><th>Queued manual submissions</th><th>Queued batch submissions</th></tr>";
echo "</tr>";
echo "<tr>";
echo "<td>$cluster_running</td><td>$cluster_queued</td><td>$web_queue</td><td>$api_queue</td>";
echo "</tr><tr><td colspan=4 class=last></tr>";
echo "</table>";
echo "</p></div>";
echo "<meta http-equiv='refresh' content='30;URL=index.php?page=status'>\n";

?>
