<?php
if (isset($_POST['ex']) && $_POST['ex'] == 'genome') {
	$release = 'Jan2015';
	$jobname = "Example Job : Genome Wide Cardiovascular";
	$gene = 'genome';
	$train = file_get_contents("Validation_Data/1.Goh_et_all/TFIDF_SVD_TestNNa/CARDIOVASCULAR/CARDIOVASCULAR_Train.txt");
	$test = "";
	$query = mysql_query("SELECT HGNC_NAME FROM `Jan2015_HGNC_ENSG_MAP`");
	while ($row = mysql_fetch_array($query)) {
		$test .= $row[0]."\n";
	}
}
elseif (isset($_POST['ex']) && $_POST['ex'] != '' && file_exists("Validation_Data/4.Literature.Genes/TFIDF_SVD_TestNNa/".$_POST['ex'])) {
	$release = 'Jan2015';
	$test = file_get_contents("Validation_Data/4.Literature.Genes/TFIDF_SVD_TestNNa/".$_POST['ex']."/TEST/Test_1.txt");
	$train = file_get_contents("Validation_Data/4.Literature.Genes/TFIDF_SVD_TestNNa/".$_POST['ex']."/TRAIN/Train_1.txt");
	$gene = $_POST['ex'];
	$jobname = "Example Job : ".$_POST['ex'];

}
else {
	$train = $test = $jobname = $notify = $method = $release = $gene = '';
	$use_pheno = 0;
}

?>
<div class=section>
<h3>Example Data</h3>
<p>The data used to benchmark pBRIT on novel disease genes is available to test the system. By selecting a gene from the list below, the corresponding data will be loaded into the submission form. Alternatively, we provide the option to perform a full genome analysis, using the cardiovascular training set of Goh <span class=italic>et al.</span></p>
<form action='index.php?page=run_example' method=POST>
<p>Select example gene: <select name=ex>
<?php
	$contents = scandir("Validation_Data/4.Literature.Genes/TFIDF_SVD_TestNNa/");
	$first = 1;
	foreach ($contents as $key => $item) {
		if ($item == '.' || $item == '..') {
			continue;
		}
		if (is_dir("Validation_Data/4.Literature.Genes/TFIDF_SVD_TestNNa/".$item)) {
			if ($gene == $item || ($gene == '' && $first == 1) ) {
				$s = 'selected';
			}
			else {
				$s = '';
			}
			$first = 0;
			echo "<option $s value='$item'>$item</value>";
		}
	}
	if ($gene == 'genome') {
		$s = 'selected';
	}
	else {
		$s = '';
	}
	echo "<option $s value='genome'>Genome Wide</option>";
	
?>
</select> <input type=submit class=button name='submit' value='Submit' ></form></p>

</div>
<?php
// don't show form if no gene is selected.
if ($gene == '') {
	exit;
}
?> 


<div class=section>
<h3>Prioritize Gene Lists</h3>

<p> pBRIT prioritizes candidate genes based on a set of training genes provided by the user. pBRIT incorporates two information-theoretic approaches for data fusion:  </p>
	<ul style="list-style-type:circle">
		<li> <b>TFIDF</b> </li>
		<li> <b>TFIDF_SVD</b> </li>
	</ul>

<p> Under each of the data fusion methods, there are two alternative regression designs:  </p>
<ul style="list-style-type:circle">
	<li> <b> Test.Pheno.Include </b> : Phenotypic information of the candidate genes is included in the regression model. </li>
	<li> <b> Test.Pheno.Discard </b> : Phenotypic information of the candidate genes is not included in the regression model. </li>
</ul>

<p> 
	In our cross-validation studies, <b>TFIDF_SVD </b> combined with <b>Test.Pheno.Include </b> gave comparatively better results.

</p>
<br><br> 
<form action='index.php?page=submit_job' method=POST>
<p><span class=emph>Set Run Parameters</span></p> 
<p class=indent><input type=text name=jobname id=jobname size=40 value='<?php echo $jobname; ?>' /> : Provide a name to identify your prioritization job. </p>
<p class=indent><input type=text name=notify id=notify size=40 /> : Optionally provide your email to recieve a notification when the prioritization is finished.</p>

<p class=indent><select style='width:10em;' name=method id=method><option selected=selected value='TFIDF_SVD'>TFIDF_SVD</option><option value='TFIDF'>TFIDF</option></select> : pBRIT datafusion method</p>
<p class=indent><select style='width:10em;' name='use_pheno' id='use_pheno'><option selected value='1'>Include</option><option  value='2'>Discard</option><select>: Include known test-gene phenotype annotations into the regression model. Discarding the phenotype information forces pBRIT to purley predict the phenotype association on the functional annotations.</p>
<?php 
$q = mysql_query("SELECT prefix FROM `Annotation_Releases` ORDER BY time_added DESC");
echo "<p class=indent><select style='width:10em;' name='db_version' id='db_version'>";
while ($row = mysql_fetch_array($q)) {
	if ($row[0] == $release) {
		$s = 'selected';
	}
	else {
		$s = '';
	}
	echo "<option $s value='".$row[0]."'>$row[0]</option>";
}
echo "</select> : Database Release. The publication was based on the January 2015 release.</p>";
?>
<!-- comment this out to disable the beta code -->
<?php
// some comment on how beta differs, edit if needed
//$title = 'Current beta code is aimed at performance of pre-processing. No changes were made to the prioritization itself.';
//echo "<p class=indent><input type=checkbox title='$title' name='beta' value='1'> Use beta code</p>";
?>
<!-- End of BETA activation black -->


<p>
	<table  cellspacing=0 width='100%'>
		<tr><th >Training Genes</th><th >Test Genes</th></tr>
		<tr>
			<td>A set of genes known to be related to the phenotype of the patient. These genes will be used to train the model. Please provide at least "3" Training genes.</td>
			<td>A set of genes affected in the patient. These genes will be ranked on their similarity to the training set.</td>
		</tr>

		<tr>
			<td>Required format: GeneIDs according to HUGO, one per line.</td>
			<td>Required format: GeneIDs according to HUGO, one per line.</td>
		</tr>
		<tr>
		  <td ><textarea id=trainingset name=trainingset cols=70 rows=25 ><?php echo $train; ?></textarea></td>
		  <td><textarea id=testset name=testset cols=70 rows=25 ><?php echo $test; ?></textarea></td>
		</tr>
		<tr>
			<td colspan=2 class=last>&nbsp;</td>
		</tr>
	</table>
</p>
<p class=indent><input class=required id=email name=email value='Provide your email' size=40 /> <input type=submit class=button name=submit id=submit value='Submit'/>
</p>
</form>
</p>
