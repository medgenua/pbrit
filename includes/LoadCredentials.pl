#!/bin/perl
# change this if you want to place this file somewhere else
# credpath is path of this pl script.
$credpath =~ s/(.*)LoadCredentials\.pl/$1/;
my $file = "$credpath.credentials";
# read in config file
our %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
## username and password 
our $userid = $config{'DBUSER'};
our $dbuser = $userid;
our $userpass = $config{'DBPASS'};
our $dbpass = $userpass;
our $host = $config{'DBHOST'};
our $dbhost = $host;
our $scriptuser = $config{'SCRIPTUSER'};
our $scriptpass= $config{'SCRIPTPASS'};
our $db = $config{'DBNAME'};
## paths
our $web_location = $config{'WEB_LOCATION'};
## HPC
our $queue = 'batch';
our $account = '';
if ($usehpc == 1) {
	if (defined($config{'QUEUE'}) && $config{'QUEUE'} ne '') {
		$queue = $config{'QUEUE'};
	}
	if (defined($config{'ACCOUNT'}) && $config{'ACCOUNT'} ne '') {
		$account = $config{'ACCOUNT'};
	}
}

our $max_on_queue = 1;
if (defined($config{'MAX_ON_QUEUE'})) {
	$max_on_queue = $config{'MAX_ON_QUEUE'};
}

## you need to manually connect to DB, as it requires host (set here) as well as db-name (not set here) ! 

# exit with success code
1;
