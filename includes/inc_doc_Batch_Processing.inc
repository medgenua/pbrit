<?php 
// for syntax highlighting
?>

<p class=indent>pBRIT can be accessed using an API. Using the script below, it is possible to submit multiple gene sets at once to pBRIT and to fetch the results automatically when they are available. As such pBRIT can be incorporated in third party tools and pipelines. <a href='scripts/Submit_by_API.py'>Right-click</a> to download the script. An example dataset with example commands to use the API is available for <a href='pBRIT_Intro_files/pBRIT_Example.tar.gz'>download here</a>.</p>
<div class=block><div class=code>Code</div>
<pre>
<?php
include("scripts/Submit_by_API.py");
?>
</pre>
</div>

<p>The following example command submits a set of 10 gene-sets for ranking based on a single set of training genes: All parameters are explained in the help function of the script above.</p>
<div class=block style='width:75%'><div class=code>Code</div>
<pre> python Submit_by_API.py -T Training_set_ext_30.txt -I BatchFile.txt -m TFIDF -e my.name@email.com -p 1 -d Jan2015</pre>
</div>

