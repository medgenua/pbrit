<div class='section'>
<h3>Welcome to the pBRIT gene prioritization portal</h3>
<p>pBRIT stands for priortization of candidate genes using <u>B</u>ayesian <u>R</u>idge Regression and <u>I</u>nformation <u>T</u>heoretic Model. It is an adaptive and scalable prioritzation tool that integrates Pubmed Abstracts, Gene Ontology, Pathways (Biocarta, EHMN, HumanCyc, INOH, KEGG, NetPath, PharmGKB, PID, Reactome, Signalink, SMPDB, WikiPathways), Protein-Protein Interactions (PhosphoPOINT, PDZBase, NetPath, PINdb, BIND, CORUM, Biogrid, InnateDB, MIPS-MPPI, Spike, Manual upload, MatrixDB, DIP, IntAct, MINT, PDB, HPRD), Protein sequence similarities (BLAST), Mouse Phenotype Ontologies (MPO), Human Phenotype Ontology (HPO), Disease Ontology (DO), Genetic association database (GAD), HuGe. pBRIT is available as web-interface for single exome prioritization or as an API for &gt;1000 exomes in batch mode.
</p>
<p>Usage instructions and example data are available in the <a href='index.php?page=documentation'>documentation</a>. (or <a href='http://143.169.238.105/pbrit/pBRIT_Intro_files/pBRIT_Example.tar.gz'>here</a>)</br>

<h3>Citing</h3>
<p>When you use pBRIT in your research, please cite the following paper: </p>
<p>Ajay Anand Kumar, Lut Van Laer, Maaike Alaerts, Amin Ardeshirdavani, Yves Moreau, Kris Laukens, Bart Loeys, Geert Vandeweyer; pBRIT: Gene Prioritization by Correlating Functional and Phenotypic Annotations Through Integrative Data Fusion, Bioinformatics, , bty079, <a href='https://doi.org/10.1093/bioinformatics/bty079'>https://doi.org/10.1093/bioinformatics/bty079</a></p>

<!-- list some notifications -->
<h3 style='color:#B40404'>NEWS:</h3>
<p><ul>
	<li><span class=italic>2018-02-14:</span> Our manuscript has been accepted by Bioinformatics. It's available <a href='https://academic.oup.com/bioinformatics/advance-article/doi/10.1093/bioinformatics/bty079/4857360?guestAccessKey=4b4f484f-7fe1-4ff4-9738-fcbc4936bb83' target=_blank> --&gt;here &lt;--</a>.
	<li><span class=italic>2017-09-20:</span> Uploaded a new Example data set for API usage: <a href='index.php?page=documentation&t=Batch_Processing'>Here</a>.</li>
	<li><span class=italic>2017-02-12:</span> Moved adapted processing from beta to main.</li>
	<li><span class=italic>2016-10-29:</span> BETA function activated : processing time of large test sets (&gt; 1000 genes) was reduced from days to approximately 30 minutes. </li>
	<li><span class=italic>2016-10-29:</span> Bug fixed in queue processing. A number of jobs did not get submitted over the last few weeks. We fixed it and submitted the jobs now.</li> 
</ul></p>
<!-- the basics -->
<h3>pBRIT Principles: </h3>
<p> The main principle behind pBRIT is that genes that share similar functional relationship might also share similar phenotypic relationship. pBRIT has three stages: 	<ol>	
			<li>Unsupervised approach for feature mining and assigning statistical weights using TF-IDF and computing gene-by-gene similarity profiles using TF-IDF and TF-IDF <span>&#8594;</span>  SVD </li>
			<li>Early integration for genomic data fusion </li>
			<li>Supervised approach i.e Bayesian ridge regression which is a discriminatory model to prioritize candidate genes. </li>
	</ol>
</p>

<h3>pBRIT Workflow</h3>
<div class='w20 toleft'>
<p><span class=emph>Stage 1:</span> The annotation sources are categorized under functional and phenotypic annotation sources. All the annotation matrices are represented as sparse binary matrices for fast and effective feature mining in runtime memory. An unsupervised approach towards feature mining is applied by assigning statistical weights to features using TF-IDF calculations. </p>

<p >For modelling co-occurrences and latent semantic dependencies between the features and its effect in overall prioritization we applied Singular Value Decomposition (SVD) on TF-IDF computed annotation matrices. This is denoted as TF-IDF&rarr;SVD or TF-IDF transformed using SVD.</p>

<p style='margin-top:5em;'><span class=emph>Stage 2:</span>  We obtain gene-by-gene proximity profiles for each of these annotation sources and create a composite matrix by taking inter-matrix means over all functional or phenotypic annotation category. The composite matrices are the final similarity matrices incorporated into the regression model in the next stage.</p>

<p style='margin-top:25em;'><span class=emph>Stage 3:</span>  For a given set of training genes, the gene-by-gene similarity matrix are retrieved under the following regression design settings:</p>

<p class='formula'>Y<sub>Pheno</sub> = &beta; X<sub>Func</sub> + &epsilon;&#126;N(0,&sigma;<sub>&epsilon;</sub><sup style='margin-left:-0.35em'>2</sup>)</p> 

<p>We incorporate ridge regression under a Bayesian framework to model the phenotype concordance scores, based on the functional annotations, for the given training genes.</p>

<p><span class=italic>p(&ycirc;</span>|<span class=bold>X</span>,<span class=italic>&beta;,&sigma;<sub>&beta;</sub>,&sigma;<sub>&epsilon;</sub></span>) denotes the expected phenotypic concordance scores with respect to available functional information. The input test genes are then prioritized by sorting this predicted phenotypic concordance score.</p>

<p style='margin-top:5em';><span class=emph>Regression Details</span>:  The performance of pBRIT depends upon two aspects:<ol>

<li>Feature mining: Incorporation of TF-IDF or TF-IDF&rarr;SVD based feature mining.<br/><br/></li>
<li>Regression design: Effect of incorporating phenotype information of the test genes into the regression model (TestNNa vs TestALLNa)</li>
</ol>
</div>
<div class='w80 toright'><img src ='images/content/pBRIT_Web_Final.png' style="width:100%"/></div>
<br style='clear:both;'/>

</div>

