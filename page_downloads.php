<?php
echo "<div class=section>";
echo "<h3>Downloads: </h3>";

echo "<p class=emph>Local Installation:</p>";
echo "<p>To setup a local installation follow these steps:<ol>";
echo "  <li>Clone our repository at <a href='https://bitbucket.org/medgenua/pbrit'>BitBucket</a>. </li>";
echo "  <li>Copy the '.credentials.example' under 'includes' to '.credentials' and adapt to your environment.</li>";
echo "  <li>Install the database at the server specified in the .credentials file:<ol>";
echo "		<li>Download database structure: <a href='pBRIT_Database/pbrit.structure.sql.gz'>Download SQL</a>";
echo "		<li>Import SQL : 'mysql -u %pbrituser% -p -h %db_host% pbrit <  pbrit.structure.sql'</li>";
echo "		<li>Download database contents: <a href='pBRIT_Database/pbrit.data.sql.gz'>Download SQL</a>";
echo "		<li>Import SQL : 'mysql -u %pbrituser% -p -h %db_host% pbrit <  pbrit.data.sql'</li>";
echo "   </ol></li>";
echo "</ol>";

echo "<p class=emph>Validation Data</p>";
echo "<p>The complete crossvalidation data can be downloaded : <a href='Validation_Data.tar.gz'>here</a></p>";


		
