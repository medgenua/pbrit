<?php
require_once 'API.class.php';
class MyAPI extends API
{
    protected $User;

    public function __construct($request) {
        parent::__construct($request);
		
    }

     /////////////////////////
     // AVAILABLE ENDPOINTS //
     /////////////////////////
	// submit 
	// check_status per job.
	// check queue status
	// get result per job.
     protected function help() {
	// some documentation.
	$result = array();
/*
        $help = "Required parameters:\n";
	$help .= "  train_file : a file containing HUGO/emsembl symbols of the training genes\n";
	$help .= "  test_file : a file containing HUGO/ensembl symbols of the test genes\n";
	$help .= "  method : either TFIDF or TFIDF-SVD (default)\n";
	$help .= "  job_name : identifier for the prioritization job. Defaults to timestamp.\n";
	$help .= "  email : optional: send email to this address on job completion.\n";
	$help .= "\nResult:\n";
	$help .= "  job_key : identifier string to retrieve results\n";
	$help .= "  comments: some information about the submission\n";
	$help .= "  ERROR : If provided, holds information on why the job was not submitted\n";
*/
        # links:
	$result['links'] = array();
	$Submit = array('href' => '/Submit', 'goal' => 'create job', 'method'=>'POST');
	array_push($result['links'],$Submit);
	$Db = array('href'=>'/GetDbVersions', 'goal' => 'list available builds', 'method'=>'GET');
	array_push($result['links'],$Db);
	//$genes = array('href' => '/ValidateGenes','goal'=>'Validate gene list','method'=>'GET');
	//array_push($result['links'],$genes);
	$list = array("href"=>'/ListJobs','goal'=>'list all jobs available to a specified user','method'=>'GET');
	array_push($result['links'],$list);
	$results = array("href"=>'/GetResults','goal'=>'get job results','method'=>'GET');
	array_push($result['links'],$results);
	$status = array("href"=>'/GetStatus','goal'=>'get job status','method'=>'GET');
	$hashedip = array("href"=>'/GetHashedUserIP','goal'=>'hash user ip for usage in job-identifiers','method'=>'GET');
	array_push($result['links'],$status);
	array_push($result['links'],$hashedip);	
	return($result);
     }
     /*********************/
     /** SUBMIT new JOB  **/
     /*********************/

     protected function Submit($args) {

	$result = array();
	// some documentation.
        $help = "Required parameters:\n";
	$help .= "  train_file : a file containing HUGO/emsembl symbols of the training genes\n";
	$help .= "  test_file : a file containing HUGO/ensembl symbols of the test genes\n";
	$help .= "  method : either TFIDF or TFIDF-SVD (default)\n";
	$help .= "  job_name : identifier for the prioritization job. Defaults to timestamp.\n";
	$help .= "  email : optional: send email to this address on job completion.\n";
	$help .= "  use_pheno : optional. Include (1,default) or exclude (2) test gene phenotype annotations in the regression.\n";
	$help .= "  db_version : optional. Specify an annotation release. Defaults to most recent version. \n";
	$help .= "\nResult:\n";
	$help .= "  job_key : identifier string to retrieve results\n";
	$help .= "  comments: some information about the submission\n";
	$help .= "  ERROR : If provided, holds information on why the job was not submitted\n";

	$result['doc'] = array('href'=>$this->api_url.'/Submit','method'=>'POST','goal'=>'create new job','text'=>$help);

        if ($this->method == 'POST') {
	    if (strtolower($this->verb) == 'help') {
		return($result);
	    }
	    // check file upload.
	    /////////////////////
	    if ($_FILES['train_file']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['train_file']['tmp_name'])) {
	    	$train_genes_file = $_FILES['train_file']['tmp_name'];
	    }
	    else {
		$result['ERROR'] = "upload of Training set failed";
		$result['help'] = $help;
		return($result);
	    }	
	    if ($_FILES['test_file']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['test_file']['tmp_name'])) {
	    	$test_genes_file = $_FILES['test_file']['tmp_name'];
	    }
	    else {
		$result['ERROR'] = "upload of Test set failed";
		$result['help'] = $help;
		return($result);
	    }
	    // check if at least one valid train & test gene is provided
	    $train_set = $this->ValidateGenes(file_get_contents($train_genes_file));
	    if (count($train_set['good']) == 0) {
		$result['ERROR'] = "No valid training genes found.";
		$result['help'] = $help;
		return($result);
	    }	
	    $test_set = $this->ValidateGenes(file_get_contents($test_genes_file));
 	    if (count($test_set['good']) == 0) {
		$result['ERROR'] = "No valid test genes found.";
		$result['help'] = $help;
		return($result);
	    }	
	    // process provided params.
	    ////////////////////////////
	    $result['comments'] = array(); 
	    $ip = $this->GetHashedUserIP();	
	    $ip = $ip['ip'];
	    // job name
	    if (isset($_POST['job_name']) && $_POST['job_name'] != '') {
		$job_name = addslashes($_POST['job_name']);
	    }
            else {
		$job_name = date('Y-m-d : H:i:s',$job_id);
	    }
	    array_push($result['comments'], "Job Name : $job_name");
	    // user email
	    if (isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		$email = $_POST['email'];
		array_push($result['comments'], "Email Notification activated : $email");
	    }
	    else {
		$email = '';
	    }
 	    // analysis method
	    if (!isset($_POST['method'])) {
		$method = 'TFIDF_SVD';
		array_push($result['comments'], "Analysis method set to default of 'TFIDF-SVD'");
	    } elseif (strtolower($_POST['method']) == 'tfidf') {
		$method = 'TFIDF';
		array_push($result['comments'], "Analysis method set to 'TFIDF'");
	    } elseif (strtolower($_POST['method']) == 'tfidf-svd') {
		$method = 'TFIDF_SVD';
		array_push($result['comments'], "Analysis method set to 'TFIDF-SVD'");
	    } else {
		$method = 'TFIDF_SVD';
		array_push($result['comments'], "Analysis method set to default of 'TFIDF-SVD'");
	    }
	    // phenotype option
	    if (!isset($_POST['use_pheno']) || ($_POST['use_pheno'] != 1 && $_POST['use_pheno'] != 2)) {
		$use_pheno = 1;
		array_push($result['comments'],"Include Phenotype annotations of test genes set to active (default)");
	    }
	    else {
		$use_pheno = $_POST['use_pheno'];
	    }
	    // DB-Version
	    if (!isset($_POST['db_version'])) {
		$_POST['db_version'] = 'na';
	    }
	    $dbv = $this->GetDbVersions();
	    if (in_array($_POST['db_version'],$dbv['builds'])) {
		$db_version = $_POST['db_version'];
		array_push($result['comments'], "Using annotation release '$db_version'");
	    }
	    else {
		$db_version = $dbv['builds'][0];
		array_push($result['comments'], "Using most recent annotation release '$db_version'");
	    }	

	    // create job files.
		$job_id = time();
	    	$job_dir = "../job_files/$ip/$job_id";
		while (file_exists($job_dir)) {
			$job_id = time();
		    	$job_dir = "../job_files/$ip/$job_id";
		}
	    	$job_key = "$ip/$job_id";
		mkdir($job_dir,0777,true);
	   
		$job_dir = realpath($job_dir);
		$job_contents = file_get_contents("../Analysis_Files/job_template.txt");
		$job_contents = str_replace('<job_ip>',$ip,$job_contents);
		$job_contents = str_replace('<job_dir>',$job_dir,$job_contents);
		$job_contents = str_replace('<job_id>',$job_id,$job_contents);
		$job_contents = str_replace('<method>',$method,$job_contents);
		$job_contents = str_replace('<use_pheno>',$use_pheno,$job_contents);
		$job_contents = str_replace('<db_version>',$db_version,$job_contents);
		$job_contents = str_replace('<script_dir>',$this->config['SCRIPTDIR'],$job_contents);
		// use beta version?
		if (isset($_POST['beta']) && $_POST['beta'] == 1) {
			$job_contents = str_replace('#beta#','',$job_contents);
			$job_contents = str_replace('pBRIT.R','pBRIT.beta.R',$job_contents);
		}

		$fh = fopen("$job_dir/job.sh",'w');
		fwrite($fh,$job_contents);
		fclose($fh);
		// write email to job_dir
		$fh = fopen("$job_dir/email","w");
		fwrite($fh,"$email");
		fclose($fh);
		// create status file
		$fh = fopen("$job_dir/status","w");
		fwrite($fh,"0");
		fclose($fh);
		// write job name.
		$fh = fopen("$job_dir/job_name","w");
		fwrite($fh,$job_name);
		fclose($fh);
		// write use_pheno flag.
		$fh = fopen("$job_dir/use_pheno","w");
		fwrite($fh,$use_pheno);
		fclose($fh);
		// write db_version .
		$fh = fopen("$job_dir/db_version","w");
		fwrite($fh,$db_version);
		fclose($fh);
		//write method
		$fh = fopen("$job_dir/method","w");
		fwrite($fh,$method);
		fclose($fh);

		// write training genes
		$fh = fopen("$job_dir/train_genes.txt","w");
		fwrite($fh,implode("\n",$train_set['good']));
		fclose($fh);
		array_push($result['comments'],"Nr of training genes: ".count($train_set['good']));
		// print excluded training genes
		if (count($train_set['bad']) > 0) {
			array_push($result['comments'],"Nr of rejected training genes: ".count($train_set['bad']));
			$fh = fopen("$job_dir/bad_train_genes.txt","w");
			foreach($train_set['bad'] as $entry) {
				fwrite($fh,"$entry\n");
			}
			fclose($fh);
		}

		// write training genes
		$fh = fopen("$job_dir/test_genes.txt","w");
		fwrite($fh,implode("\n",$test_set['good']));
		fclose($fh);
		array_push($result['comments'],"Nr of test genes: ".count($test_set['good']));
		// print excluded test genes
		if (count($test_set['bad']) > 0) {
			array_push($result['comments'],"Nr of rejected test genes: ".count($test_set['bad']));
			$fh = fopen("$job_dir/bad_test_genes.txt","w");
			foreach($test_set['bad'] as $entry) {
				fwrite($fh,"$entry\n");
			}
			fclose($fh);
		}
	

		# make writable.
		system("chmod -R 777 $job_dir");
		$fh = fopen("../job_queue/api_queue",'a');
		if (flock($fh, LOCK_EX)) {
		 	fwrite($fh,"$job_dir\n");
			flock($fh, LOCK_UN); // unlock the file
			fclose($fh);
			$result['job_key'] = $job_key;
			return($result);
		} else {
			// flock() returned false, no lock obtained
			$result['ERROR'] = "Could not lock queue file. Job was NOT submitted!";
			return($result);
			exit();
		}
	} else {
	    $result['ERROR'] = 'Only POST is accepted';
            return $result;
        }

     }

     /******************************/
     /* LIST AVAILABLE DB VERSIONS */
     /******************************/
     protected function GetDbVersions() {
	$this->ConnectToDB() or die("Could not connect to database");
	$q = mysql_query("SELECT prefix FROM `Annotation_Releases` ORDER BY time_added DESC");
	$result = array();
	$result['doc'] = array('href'=>$this->api_url.'/GetDbVersions','method'=>'GET','goal'=>'list database builds');
	$result['builds'] = array();
	while ($r = mysql_fetch_array($q)) {
		array_push($result['builds'],$r[0]);
	}
	return($result);
     }
     /*************************/
     /* get submitter user ip */
     /*************************/
     protected function GetHashedUserIP() {
	$client  = @$_SERVER['HTTP_CLIENT_IP'];
	$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	$remote  = $_SERVER['REMOTE_ADDR'];
	$result['doc'] = array('href'=>$this->api_url.'/GetHashedUserIP','method'=>'GET','goal'=>'hash user ip for usage in job-identifiers');

	if(filter_var($client, FILTER_VALIDATE_IP)) {
        	$ip = $client;
    	}
    	elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
        	$ip = $forward;
	}
	else {
        	$ip = $remote;
    	}
	$result['ip'] = md5($ip);
    	return $result;
     }
     /*************************************/
     /* CHECK RPOVIDED GENES FOR VALIDITY */
     /*************************************/
     protected function ValidateGenes($gene_string) {
	
	$this->ConnectToDB() or die("Could not connect to database");
	// read the mapping if not available. 
	if (!isset($this->Map_h2e)) {
		$this->Map_h2e = array();
		$this->Map_e2h = array();
		$query = mysql_query("SELECT `HGNC_NAME`,`ENSG_ID` FROM `HGNC_ENSG_MAP`") ;
		while ($row = mysql_fetch_array($query,MYSQL_ASSOC)) {
			$this->Map_h2e[$row['HGNC_NAME']] = $row['ENSG_ID'];
			$this->Map_h2e[$row['ENSG_ID']] = $row['HGNC_NAME'];
		}
	}
	$result = array();
	$result['doc'] = array('href'=>$this->api_url.'/ValidateGenes/geneA,geneB,geneC','method'=>'GET','goal'=>'validate comma or newline seperated list of genes against pBRIT database');
	$result['good'] = array();
	$result['bad'] = array();
	// if provided as direct call, replace provided argument by provided list.
	if (($gene_string == '' || count($gene_string) == 0) && $this->verb != '') {
		$gene_string = $this->verb;
	}
	foreach (preg_split("/[,\n]/",rtrim($gene_string)) as $gene) {
		if ($gene == "") {
			continue;
		}
		// hugo
		if (array_key_exists($gene, $this->Map_h2e)) {
			array_push($result['good'],$gene);
			continue;
		}
		// ensembl
		if (array_key_exists($gene,$this->Map_e2h)) {
			array_push($result['good'],$gene);
			continue;
		}
		// region : todo
		if (preg_match("/^(chr)*([\dXYM]+):(\d+)-(\d+)$/",$gene,$matches)) {
			// get genes in provided region, using build hg19. TODO
			array_push($result['bad'],"$gene (regions are not supported yet)");
			continue;
		}
		// bad.
		array_push($result['bad'],"$gene (symbol not recognized in internal database)");

	}
	return($result);
     }

     /*****************/
     /** LIST JOBS **/
     /*****************/

     protected function ListJobs($args) {
	$result['doc'] = array('href'=>$this->api_url.'/ListJobs/<hashed_ip>','method'=>'GET','goal'=>'list all jobs available to current user');
	if ($this->method == 'GET') {
		$validate_user = $this->CheckUser($this->verb);
		if ($validate_user['access'] == 'denied') {
			$result['ERROR'] = $validate_user['ERROR'];
			$result['links'] = $validate_user['links'];
			return($result);
		}
		$job_ip = $this->verb;
		$result['jobs'] = array();
		// no subdir (no jobs sumbitted?)
		if (!file_exists("../job_files/$job_ip")) {
			$result['ERROR'] = 'No jobs found';
			return($result);
		}
		$jobs = array_filter(glob("../job_files/$job_ip/*"),'is_dir');
		foreach ($jobs as $job_dir) {
			//error_log($job_dir);
			$path = explode("/",$job_dir);
			$job = array();
			$job['id'] = end($path);
			$job['status'] = $this->GetStatus(array(end($path)))['status'];
			$job['name'] = rtrim(file_get_contents("$job_dir/job_name"));
			array_push($result['jobs'],$job);	
		}
		return($result);
		
        }
        else {
	  $result['ERROR'] = 'Only GET is accepted';
	  return($result);
	}
      } 
		
      ////////////////////
     // ** check user ** /
     /////////////////////
     private function CheckUser($ip) {
	$this_ip = $this->GetHashedUserIP();
	$result = array();
	if ($ip == '') {
		$result['access'] = 'denied';
		$result['links'] = array();
		array_push($result['links'], array('href'=> $this->api_url."/GetHashedUserIP",'goal'=>'get your hashed ip string','method'=>'GET'));
		$result['ERROR'] = 'No hashed IP provided.';
	}
	elseif ($ip != $this_ip['ip']) {
		$result['access'] = 'denied';
		$result['ERROR'] = 'provided IP does not match current IP. This is not allowed';
 	}
	else {
		$result['access'] = 'granted';
	}
	return($result);
     }
     /*****************/
     /** GET RESULTS **/
     /*****************/

     protected function GetResults($args) {
	$result['doc'] = array('href'=>$this->api_url.'/GetResults/<hashed_ip>/<job_id>','method'=>'GET','goal'=>'retrieve job results');
	$result['links'] = array();
	$result['links'][] = array('href'=>$this->api_url.'/GetGlobalHeatMap/<hashed_ip>/<job_id>','method'=>'GET','goal'=>'download (.png) global similarity heatmap respresenting prioritization results');
	$result['links'][] = array('href'=>$this->api_url.'/GetGeneHeatMap/<hashed_ip>/<job_id>/<test_gene>','method'=>'GET','goal'=>'download (.png) heatmap respresenting contribution of annotation sources to prioritization results of a specific gene');

	$result['links'][] = array('href'=>$this->api_url.'/GetModel/<hashed_ip>/<job_id>','method'=>'GET','goal'=>'download (.dat) the R data containing prioritization results');

	if ($this->method == 'GET') {
	      $validate_user = $this->CheckUser($this->verb);
	      if ($validate_user['access'] == 'denied') {
			$result['ERROR'] = $validate_user['ERROR'];
			return($result);
	      }
	      $job_ip = $this->verb;
	      $job_id = "na";
	      if (isset($args[0]) && is_numeric($args[0])) {
		$job_id = $args[0];
	      }
	      $job_dir = "../job_files/$job_ip/$job_id";
	      if ($job_ip == '' || $job_id == 'na') {
			$result['ERROR'] = 'no job specified';
			return($result);
	      }
	      // job exists?
	      if (!file_exists($job_dir)) {
			$result['ERROR'] = "Requested job was not found";
			return($result);
	      }
	      // job finished?
	      $status = $this->GetStatus($args)['status'];
	      if ($status != 'Finished') {
			$result['ERROR'] = "Query is not marked as finished (status = $status). Use the GetStatus/ syntax to check job status";
			return($result);
	      }
	      // construct output
	      	$result['train_genes'] = explode("\n",file_get_contents("$job_dir/train_genes.txt"));
		$result['test_genes'] = explode("\n",file_get_contents("$job_dir/test_genes.txt"));
		$result['db_version'] = rtrim(file_get_contents("$job_dir/db_version"));
		$result['job_name'] = rtrim(file_get_contents("$job_dir/job_name"));
		$result['method'] = rtrim(file_get_contents("$job_dir/method"));
		$use_pheno = rtrim(file_get_contents("$job_dir/use_pheno"));
		if ($use_pheno == 1) {
			$result['test_gene_phenotypes'] = "included";
		}
		else {
			$result['test_gene_phenotypes'] = "excluded";
		}
		$result['ranking'] = array();
		$fh = fopen("$job_dir/output_hgnc_list.txt",'r');
        	while (($line = fgets($fh)) !== false) {
			$c = explode("\t",str_replace('"','',rtrim($line)));
			$result['ranking'][$c[0]] = array('Symbol' => $c[1], 'Score' => round($c[2],5));
		}
		fclose($fh);	
		$result['excluded_test_genes'] = array();
		if (file_exists("$job_dir/bad_test_genes.txt")) {
			$result['excluded_test_genes'] = explode("\n",file_get_contents("$job_dir/bad_test_genes.txt"));
		}
		$result['excluded_train_genes'] = array();
		if (file_exists("$job_dir/bad_train_genes.txt")) {
			$result['excluded_train_genes'] = explode("\n",file_get_contents("$job_dir/bad_train_genes.txt"));
		}
		// set of links:
		$result['links'] = array();
		// R object.
		$result['links'][] = array("name"=>'full_model',"href"=>$this->api_url."/GetModel/$job_ip/$job_id","method"=>"GET","goal"=>"get R object of the full pBRIT model");
		// heatmap
		$result['links'][] = array("name"=>'heatmap', "href"=> $this->api_url."/GetGlobalHeatMap/$job_ip/$job_id","method"=>"GET","goal"=>"get global similarity heatmap");
		// per test gene
		$result['links'][]= array("name"=>'Contributions', "href"=> $this->api_url."/GetGeneHeatMap/$job_ip/$job_id/<Test_Gene>","method"=>"GET","goal"=>"get Annotation source contribution heatmap");

		return($result);
	}
	else {
		$result['ERROR'] = "Only accepts GET requests";
		return($result);
        }

		
	
     }
     protected function GetModel($args) {
	$result['doc'] = array('href'=>$this->api_url.'/GetModel/<hashed_ip>/<job_id>','method'=>'GET','goal'=>'retrieve R object of the full pBRIT model.', 'note' => 'Output is a binary file (pBRIT_fm_object.dat) if successfull');
	if ($this->method == 'GET') {
	      $validate_user = $this->CheckUser($this->verb);
	      if ($validate_user['access'] == 'denied') {
			$result['ERROR'] = $validate_user['ERROR'];
			return($result);
	      }
	      $job_ip = $this->verb;
	      $job_id = "na";
	      if (isset($args[0]) && is_numeric($args[0])) {
		$job_id = $args[0];
	      }
	      $job_dir = "../job_files/$job_ip/$job_id";
	      if ($job_ip == '' || $job_id == 'na') {
			$result['ERROR'] = 'no job specified';
			return($result);
	      }
	      // job exists?
	      if (!file_exists($job_dir)) {
			$result['ERROR'] = "Requested job was not found";
			return($result);
	      }
	      // job finished?
	      $status = $this->GetStatus($args)['status'];
	      if ($status != 'Finished') {
			$result['ERROR'] = "Query is not marked as finished (status = $status). Use the GetStatus/ syntax to check job status";
			return($result);
	      }
	      // file exists?
	      if (file_exists("$job_dir/pBRIT_fm_object.dat")) {
		header('Content-type: application/octet-stream');
		header('Content-Length:'.filesize("$job_dir/pBRIT_fm_object.dat"));
		header('Content-Disposition: attachment; filename="pBRIT_fm_object.dat"');
		$data = file_get_contents("$job_dir/pBRIT_fm_object.dat");
		echo $data;
		exit;
	      }
	      else {
		$result['ERROR'] = 'pBRIT model file not found';
		return($result);
	      }
	}
	else {	
		$result['ERROR'] = "Only accepts GET requests";
		return($result);
        }
     }
     protected function GetGlobalHeatMap($args) {
	$result['doc'] = array('href'=>$this->api_url.'/GetModel/<hashed_ip>/<job_id>','method'=>'GET','goal'=>'retrieve heatmap of global prioritization results', 'note' => 'Output is a png file (pBRIT_global_heatmap.png) if successfull');
	if ($this->method == 'GET') {
	      $validate_user = $this->CheckUser($this->verb);
	      if ($validate_user['access'] == 'denied') {
			$result['ERROR'] = $validate_user['ERROR'];
			return($result);
	      }
	      $job_ip = $this->verb;
	      $job_id = "na";
	      if (isset($args[0]) && is_numeric($args[0])) {
		$job_id = $args[0];
	      }
	      $job_dir = "../job_files/$job_ip/$job_id";
	      if ($job_ip == '' || $job_id == 'na') {
			$result['ERROR'] = 'no job specified';
			return($result);
	      }
	      // job exists?
	      if (!file_exists($job_dir)) {
			$result['ERROR'] = "Requested job was not found";
			return($result);
	      }
	      // job finished?
	      $status = $this->GetStatus($args)['status'];
	      if ($status != 'Finished') {
			$result['ERROR'] = "Query is not marked as finished (status = $status). Use the GetStatus/ syntax to check job status";
			return($result);
	      }
	      // file exists?
	      if (file_exists("$job_dir/ranking_heatmap.png")) {
		header('Content-Type: image/png');
		header('Content-Length:'.filesize("$job_dir/ranking_heatmap.png"));
		header('Content-Disposition: attachment; filename="pBRIT_global_heatmap.png"');
		$data = file_get_contents("$job_dir/ranking_heatmap.png");
		echo $data;
		exit;
	      }
	      else {
		$result['ERROR'] = 'Global ranking heatmap not found';
		return($result);
	      }
	}
	else {	
		$result['ERROR'] = "Only accepts GET requests";
		return($result);
        }
     }
     protected function GetGeneHeatMap($args) {
	$result['doc'] = array('href'=>$this->api_url.'/GetModel/<hashed_ip>/<job_id>/<test_gene>','method'=>'GET','goal'=>'retrieve heatmap of global prioritization results', 'note' => 'Output is a png file (pBRIT_<gene>_heatmap.png) if successfull');
	if ($this->method == 'GET') {
	      $validate_user = $this->CheckUser($this->verb);
	      if ($validate_user['access'] == 'denied') {
			$result['ERROR'] = $validate_user['ERROR'];
			return($result);
	      }
	      $job_ip = $this->verb;
	      $job_id = "na";
	      if (isset($args[0]) && is_numeric($args[0])) {
		$job_id = $args[0];
	      }
	      $job_dir = "../job_files/$job_ip/$job_id";
	      if ($job_ip == '' || $job_id == 'na') {
			$result['ERROR'] = 'no job specified';
			return($result);
	      }
	      // job exists?
	      if (!file_exists($job_dir)) {
			$result['ERROR'] = "Requested job was not found";
			return($result);
	      }
	      // job finished?
	      $status = $this->GetStatus($args)['status'];
	      if ($status != 'Finished') {
			$result['ERROR'] = "Query is not marked as finished (status = $status). Use the GetStatus/ syntax to check job status";
			return($result);
	      }
	      // test gene specified?
	      $gene = '';
	      if (isset($args[1]) && $args[1] != '') {
			$gene = $args[1];
	      }
	      else {
			$result['ERROR'] = "No Test Gene specified";
			return($result);
	      }
	      // valid gene?
	      $test_genes = preg_split("/[\n\t]/",file_get_contents("$job_dir/test_mapping.txt")); 
	      if (!in_array($gene,$test_genes)) {
			$result['ERROR'] = "Provided Test Gene was not found in final results";
			return($result);
	      }
	      // try to create heatmap if not existing.
	      if (!file_exists("$job_dir/gene_heatmaps/$gene.png")) {
			$db_version = rtrim(file_get_contents("$job_dir/db_version"));
			$method = rtrim(file_get_contents("$job_dir/method"));
			system("/opt/software/R/3.2.1/bin/Rscript '$job_dir/../../../Analysis_Files/extractAnnotationMatrix.R' '$gene' '$job_dir/' '$method' '$db_version'");
	      }
	      // generate if existing.
	      if (file_exists("$job_dir/gene_heatmaps/$gene.png")) {
		header('Content-Type: image/png');
		header('Content-Length:'.filesize("$job_dir/gene_heatmaps/$gene.png"));
		header('Content-Disposition: attachment; filename="pBRIT_'.$gene.'_heatmap.png"');
		$data = file_get_contents("$job_dir/gene_heatmaps/$gene.png");
		echo $data;
		exit;
	      }
	      else {
		$result['ERROR'] = 'Contributions heatmap for '.$gene.' could not be generated';
		return($result);
	      }
	}
	else {	
		$result['ERROR'] = "Only accepts GET requests";
		return($result);
        }
     }
     /*********************/
     /** GET JOB STATUS **/
     /*******************/
     protected function GetStatus($args) {
        if ($this->method == 'GET') {
	      $result = array('status' => '');
	      $validate_user = $this->CheckUser($this->verb);
	      if ($validate_user['access'] == 'denied') {
			$result['ERROR'] = $validate_user['ERROR'];
			return($result);
	      }
	      $job_ip = $this->verb;
	      $job_id = "na";
	      if (isset($args[0]) && is_numeric($args[0])) {
		$job_id = $args[0];
	      }
	      $job_dir = "../job_files/$job_ip/$job_id";
	      $result['doc'] = array('href'=>$this->api_url.'/GetStatus/<hashed_ip>/<job_id>','method'=>'GET','goal'=>'Get Job Status');
	      // job specified
	      if ($job_ip == '' || $job_id == 'na') {
			$result['ERROR'] = 'no job specified';
			return($result);
	      }
	      // job exists?
	      if (!file_exists($job_dir)) {
			//return(array('ERROR' => "Requested job was not found"));
			$result['ERROR'] = "Requested job was not found";
			return($result);
			
	      }
	      // get status.
	      $status = rtrim(file_get_contents("$job_dir/status"));
	      if ($status == 1) {
		//return(array('status' => 'Finished'));
		$result['status'] = 'Finished';
	      }
	      elseif ($status == 0) {
		//return(array('status' => 'Queued'));
		$result['status'] = 'Queued';
	      }
	      elseif ($status == -1) {
		//return(array('status' => 'Failed'));
		$result['status'] = 'Failed';
	      }
	      return($result);
		
	} else {
            return "Only accepts GET requests";
        }
     }
     
}

// GET posted request.
if (isset($_GET['request'])) {
	$request = $_GET['request'];
} elseif (isset($_POST['request'])) {
	$request = $_POST['request'];
}
else {
	$request = 'help';
	//echo "No action provided. Redirecting to documentation in 3 seconds.\n";
	//echo "<meta http-equiv='refresh' content='3;URL=../index.php?page=documentation&topic=api'/>";
	//exit();
}
// INITIALIZE THE API
$myAPI = new MyAPI($request);
// PROCESS THE PROVIDED REQUEST
$var = $myAPI->processAPI();
echo $var;
?>
