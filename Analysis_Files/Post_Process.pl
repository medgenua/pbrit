#!/usr/bin/perl

# modules
use DBI;
use Cwd 'abs_path';
use File::Basename;

# variables
my ($job_ip,$job_id,$method,$use_pheno,$db_version) = @ARGV;
my $test = `cat test_genes.txt`;
my $train = `cat train_genes.txt`;
my $ranks = `cat output_hgnc_list.txt`;
my $run_time = time() - $job_id;
my $scriptdir = my $credpath = dirname(abs_path($0));

system("chmod -R 755 output_hgnc_list.txt pBRIT_fm_object.dat ranking_heatmap.png");

# connect do DB
our $credpath = "$scriptdir/";
require("$scriptdir/LoadCredentials.pl");
my %attr = ( PrintError => 1, RaiseError => 1, mysql_auto_reconnect => 1);
my $connectionInfocnv="dbi:mysql:$db:$dbhost";
my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass,\%attr) ;#or die('Could not connect to database'); ## our : make available to subs
## retry on failed connection (server overload?)
my $i = 0;
while ($i < 10 && ! defined($dbh)) {
        sleep 1;
        $i++;
        $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass,\%attr) ;
}
if (!defined($dbh)) {
       die('Could not connect to database');
}
my $sth = $dbh->prepare("INSERT INTO `Submitted_Jobs` (job_id,job_ip, train_set,test_set,db_version,use_pheno,pbrit_method,run_time,ranking_results) VALUES (?,?,?,?,?,?,?,?,?)");
$sth->execute($job_id,$job_ip,$train,$test,$db_version,$use_pheno,$method,$run_time,$ranks);
$sth->finish();
$dbh->disconnect();
exit;
