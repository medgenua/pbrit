#!/usr/bin/env bash
#PBS -N pbrit.<job_ip>.<job_id>
#PBS -A Aorta
#PBS -q Prioritization
#PBS -m n
#PBS -d <job_dir>
#PBS -o <job_dir>/job.o.txt
#PBS -e <job_dir>/job.e.txt
#PBS -l nodes=1:ppn=1,mem=4gb

echo "Running on host: "`hostname`
echo "Start Time: "`date`

#beta### in beta usage, the #beta# is removed to run specific comments
# preprocess to get the input matrices.
perl /pBRIT//Analysis_Files/Pre_Process.pl '<job_dir>' '<method>' '<use_pheno>' '<db_version>'

# the regression
/opt/software/R/3.2.1/bin/Rscript '<script_dir>/Analysis_Files/pBRIT.R' '<job_dir>/train_genes.txt' '<job_dir>/test_genes.txt' '<job_dir>/' '<use_pheno>' '<db_version>' '<method>'

# if pBRIT crashed: 
if [ "$?" -ne "0" ] ; then
   echo -1 > status
   echo "pBRIT crashed." > failed
   exit $?
fi

## post process.
perl '<script_dir>/Analysis_Files/Post_Process.pl' '<job_ip>' '<job_id>' '<method>' '<use_pheno>' '<db_version>'

echo "1" > status
echo "End Time: "`date`
