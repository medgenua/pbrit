#!/usr/bin/perl
$|++;
# modules
use DBI;
use Cwd 'abs_path';
use File::Basename;
use Parallel::ForkManager;

my ($wd,$method,$use_pheno,$db_version) = @ARGV;
my $scriptdir = my $credpath = dirname(abs_path($0));
# connect do DB
our $credpath = "$scriptdir/";
my $dbh = Get_DB_Connection();
if (!-d "$wd/data_files/"){
	mkdir("$wd/data_files/");
}
############################
# get the ensembl/hugo map.#
############################
print "Building the HUGO/ensembl map.\n";
my $sth = $dbh->prepare("SELECT * FROM `$db_version"."_HGNC_ENSG_MAP`");
my $nr = $sth->execute();
my $max = ($nr < 50000 ? $nr : 50000);
my $rowcache;
my %h_x_e = (); #hgnc_to_ensembl 
my %e_x_h = (); #ens_to_hgnc;
my %double_h_x_e = () ;# keep track of seen items for removal of double mappings.
while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
	$result->[0] =~ s/\s//g;
	$result->[1] =~ s/\s//g;
	# discard  symbols with double mappings.
	if (defined($h_x_e{$result->[0]})) {
		$double_h_x_e{$result->[0]} = 1;
		#delete($h_x_e{$result->[0]});
		next;
	}
	# store the mapping
	$h_x_e{$result->[0]} = $result->[1];
	$e_x_h{$result->[1]}{$result->[0]} = 0;
}
$sth->finish();
$dbh->disconnect();
####################################
# Validate training and test genes #
####################################
print "Validating input gene symbols\n";
my $non_id = ();
my @tr = `cut -f 1 $wd/train_genes.txt`;
chomp(@tr);
my @ens_tr = ();
my %tr_seen = ();
foreach my $symbol (@tr) {
	$symbol =~ s/\s//g;
	if (defined($double_h_x_e{$symbol})) {
		$non_id .= "$symbol\n";
		next;
	}
	if (defined($h_x_e{$symbol})) {
		next if (defined($tr_seen{$h_x_e{$symbol}})); # skip doubles.
		push(@ens_tr,$h_x_e{$symbol});
		$e_x_h{$h_x_e{$symbol}}{$symbol} = 1; # track as seen symbol.
		$tr_seen{$h_x_e{$symbol}} = 1; # track unique ens_id
	}
	else {
		$non_id .= "$symbol\n";
	}
}
# write out the ens training/test lists.
open OUT, ">$wd/data_files/training.txt";
foreach my $ens (@ens_tr) {
	foreach my $s (keys(%{$e_x_h{$ens}})) {
		if ($e_x_h{$ens}{$s} == 1) {
			print OUT "$ens,$s\n";
			last;
		}
	}
}
close OUT;

#%tr_seen = ();
my @ts = `cut -f 1 $wd/test_genes.txt`;
chomp(@ts);
my @ens_ts = ();
my %ts_seen = ();
foreach my $symbol (@ts) {
	$symbol =~ s/\s//g;
	if (defined($double_h_x_e{$symbol})) {
		$non_id .= "$symbol\n";
		next;
	}
	
	if (defined($h_x_e{$symbol})) {
		my $ens = $h_x_e{$symbol};
		# seen as training.
		if (defined($tr_seen{$ens})) {
			open OUT, ">>$wd/bad_test_genes.txt";
			foreach my $s (keys(%{$e_x_h{$ens}})) {
				if ($e_x_h{$ens}{$s} == 1) {
					print OUT "$symbol (training gene, as $s)\n";
					last;
				}
			}
			close OUT;
			next;
		}
		next if (defined($ts_seen{$h_x_e{$symbol}}));
		push(@ens_ts,$h_x_e{$symbol});
		$e_x_h{$h_x_e{$symbol}}{$symbol} = 1; # track as seen symbol.
		$ts_seen{$h_x_e{$symbol}} = 1;
	}
	else {
		$non_id .= "$symbol\n";
	}
}
# write out the ens training/test lists.
open OUT, ">$wd/data_files/test.txt";
foreach my $ens (@ens_ts) {
	foreach my $s (keys(%{$e_x_h{$ens}})) {
		if ($e_x_h{$ens}{$s} == 1) {
			print OUT "$ens,$s\n";
			last;
		}
	}
}
close OUT;

# print out non-found symbols;
open OUT, ">$wd/pBRIT_NON_ID.txt";
print OUT $non_id;
close OUT;

%tr_seen = ();
%ts_seen = ();
###################################################
## build the functional/ phenotype scores matrices.#
###################################################
our $fork = Parallel::ForkManager->new(2);

foreach my $xy (qw/X Y/) {
	foreach my $tt (qw/train test/) {
		$fork->start("$tt.$xy") and next; # use forks to force memory release. can be scaled up later to do parallel building of matrices.
		#print "Building R input matrix for $tt.$xy\n";
		getMySQLMat(\@ens_tr,\@ens_ts,$xy,$method,$wd,$tt,$db_version);
		$fork->finish();
	}

}
$fork->wait_all_children;

# make the binded tables.
print "Combining training & test tables.\n";
system("cat '$wd/data_files/train.X.txt'  > '$wd/data_files/X.txt'");
system("tail -n +2 '$wd/data_files/test.X.txt' >> '$wd/data_files/X.txt'");

system("cat '$wd/data_files/train.Y.txt' > '$wd/data_files/Y.txt'");
system("tail -n +2 '$wd/data_files/test.Y.txt' >> '$wd/data_files/Y.txt'");


## all done to start R.



#################
## SUBROUTINES ##
#################
sub getMySQLMat {
	my ($query,$target,$xy,$method,$wd,$ttflag,$db_version) = @_;
	my $dbh = Get_DB_Connection();

	#my $ens_list = $query;
	my @ens_list = ();
	if ($ttflag eq 'train') {
		@ens_list = @$query;
	}
	else {
		@ens_list = @$target;
		push(@ens_list,@$query);
	}
	my %mat = ();
	my %training = map {$_ => 1} @$query;
	my $nr_ele = scalar(@ens_list);
	my $colstring = "'".join("','",@ens_list)."'";
	my @tmp_array = @ens_list;
	my $nr_slices = int(scalar(@tmp_array) / 1000) + 1;
	print "Building '$wd/data_files/$ttflag.$xy.txt':\n";
	#print "  In Memory, using $nr_slices slices of 1000 genes: ";
	my $i = 0;
	while (my @row_array = splice @tmp_array, 0, 1000) {
		$i++;
		#print "$i..";
		my $rowstring = "'".join("','",@row_array)."'";
		my $sth = $dbh->prepare("SELECT * FROM `".$db_version."_".$method."_COMPOSITE_".$xy."` WHERE ENSG_ROW IN ($rowstring) AND ENSG_COL IN ($colstring)");
		open OUT, ">>$wd/testPBRIT.txt";
		print OUT "SELECT * FROM `".$db_version."_".$method."_COMPOSITE_".$xy."` WHERE ENSG_ROW = $rowstring AND ENSG_COL IN ($colstring)\n";
		close OUT;
		my $nr = $sth->execute();
		my $max = ($nr < 50000 ? $nr : 50000);
		my $rowcache;
		#print "  nr.rows to process : $nr\n";
		while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
			# store the score for gene_vs_traininggene
			if (defined($training{$result->[0]})) {
				$mat{$result->[1]}{$result->[0]} = $result->[2];
			}
			if (defined($training{$result->[1]})) {
				$mat{$result->[0]}{$result->[1]} = $result->[2];
			}
		}
	}
	$sth->finish();
	$dbh->disconnect();
	#print "done\n";
	## write out the matrix.
	#print "  on disk...\n";
	open OUT, ">$wd/data_files/$ttflag.$xy.txt";

	my $nr_ts = scalar(@$target);
	my $nr_tr = scalar(@$query);
			
	# header.
	print OUT "'".join("','",@$query)."'\n";
	if ($ttflag eq 'train') {
		# rows
		for (my $i = 0; $i < $nr_tr; $i++) {
			# for each row (test genes, they are on top.): print matches to the training genes (query)
			print OUT join(",",map {"'".$mat{$query->[$i]}{$_}."'"} @$query)."\n";
		}
	}
	else {
		for (my $i = 0; $i < $nr_ts; $i++) {
			# for each row (test genes, they are on top.): print matches to the training genes (query)
			print OUT join(",",map {"'".$mat{$target->[$i]}{$_}."'"} @$query)."\n";
		}
	}

	close OUT;
	#print "Done.\n";
	# clean.
	%mat = ();

}


sub Get_DB_Connection {
	require("$credpath/LoadCredentials.pl");
	my %attr = ( PrintError => 1, RaiseError => 1, mysql_auto_reconnect => 1);
	my $connectionInfo="dbi:mysql:$db:$dbhost";
	my $dbh = DBI->connect($connectionInfo,$dbuser,$dbpass,\%attr) ;
	## retry on failed connection (server overload?)
	my $i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 1;
		$i++;
		$dbh = DBI->connect($connectionInfo,$dbuser,$dbpass,\%attr) ;
	}
	if (!defined($dbh)) {
		die("Could not connect to database.");
	}
	return($dbh);
}

