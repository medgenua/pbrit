BLR_new = function (y, XF = NULL, XR = NULL, XL = NULL, GF = list(ID = NULL,A = NULL), prior = NULL, nIter = 1100, burnIn = 100, thin = 10,thin2 = 1e+10, saveAt = "", minAbsBeta = 1e-09, weights = NULL) 
{
    #welcome()
    y <- as.numeric(y)
    n <- length(y)
    if (!is.null(XR)) {
        if (any(is.na(XR)) | nrow(XR) != n) 
            stop("The number of rows in XR does not correspond with that of y or it contains missing values")
    }
    nSums <- 0
    whichNa <- which(is.na(y))
    nNa <- sum(is.na(y))
    if (is.null(weights)) {
        weights <- rep(1, n)
    }
    sumW2 <- sum(weights^2)
    mu <- weighted.mean(x = y, w = weights, na.rm = TRUE)
    yStar <- y * weights
    yStar[whichNa] <- mu * weights[whichNa]
    e <- (yStar - weights * mu)
    varE <- var(e, na.rm = TRUE)/2
    if (is.null(prior$varE)) {
        cat("==============================================================================\n")
        cat("No prior was provided for residual variance, BLR will use an improper prior.\n")
        prior$varE <- list(df = 0, S = 0)
        cat("==============================================================================\n")
    }
    post_mu <- 0
    post_varE <- 0
    post_logLik <- 0
    post_yHat <- rep(0, n)
    post_yHat2 <- rep(0, n)
    hasRidge <- !is.null(XR)
    hasXF <- !is.null(XF)
    hasLasso <- !is.null(XL)
    hasGF <- !is.null(GF[[1]])
    if (hasRidge) {
        if (is.null(prior$varBR)) {
            cat("==============================================================================\n")
            cat("No prior was provided for varBR, BLR will use an improper prior.\n")
            cat("==============================================================================\n")
            prior$varBR <- list(df = 0, S = 0)
        }
        for (i in 1:n) {
            XR[i, ] <- weights[i] * XR[i, ]
        }
        pR <- ncol(XR)
        xR2 <- colSums(XR * XR)
        bR <- rep(0, pR)
        namesBR <- colnames(XR)
        varBR <- varE/2/sum(xR2/n)
        post_bR <- rep(0, pR)
        post_bR2 <- post_bR
        post_varBR <- 0
        XRstacked <- as.vector(XR)
        rm(XR)
    }
    time <- proc.time()[3]
    for (i in 1:nIter) {
        if (hasRidge) {
            ans <- .Call("sample_beta", n, pR, XRstacked, xR2,bR, e, rep(varBR, pR), varE, minAbsBeta)
            bR <- ans[[1]]
            e <- ans[[2]]
            SS <- crossprod(bR) + prior$varBR$S #t(bR) %*% bR
            df <- pR + prior$varBR$df
            #cat("degree of freedom: ",df,"\n")
            varBR <- SS/rchisq(df = df, n = 1)
        }
        e <- e + weights * mu
        rhs <- sum(weights * e)/varE
        C <- sumW2/varE
        sol <- rhs/C
        mu <- rnorm(n = 1, sd = sqrt(1/C)) + sol
        e <- e - weights * mu
        SS <- crossprod(e) + prior$varE$S
        df <- n + prior$varE$df
        varE <- as.numeric(SS)/rchisq(n = 1, df = df)
        sdE <- sqrt(varE)
        yHat <- yStar - e
        if (nNa > 0) {
            e[whichNa] <- rnorm(n = nNa, sd = sdE)
            yStar[whichNa] <- yHat[whichNa] + e[whichNa]
        }
        if ((i%%thin == 0)) {
            tmp <- c(varE)
            #fileName <- paste(saveAt, "varE", ".dat", sep = "")
            #write(tmp, ncolumns = length(tmp), file = fileName, append = TRUE, sep = " ")
            if (hasRidge) {
                tmp <- varBR
                #fileName <- paste(saveAt, "varBR", ".dat", sep = "")
                #write(tmp, ncolumns = length(tmp), file = fileName,append = TRUE, sep = " ")
            }
            if (i >= burnIn) {
	        #cat("inside 2nd if\t",i,"\t",burnIn,"\n")
                nSums <- nSums + 1
                k <- (nSums - 1)/(nSums)
                tmpE <- e/weights
                tmpSD <- sqrt(varE)/weights
                if (nNa > 0) {
                  tmpE <- tmpE[-whichNa]
                  tmpSD <- tmpSD[-whichNa]
                }
                logLik <- sum(dnorm(tmpE, sd = tmpSD, log = TRUE))
                post_logLik <- post_logLik * k + logLik/nSums
                post_mu <- post_mu * k + mu/nSums
                post_varE <- post_varE * k + varE/nSums
                post_yHat <- post_yHat * k + yHat/nSums
                post_yHat2 <- post_yHat2 * k + (yHat^2)/nSums
                if (hasRidge) {
                  post_bR <- post_bR * k + bR/nSums
                  post_bR2 <- post_bR2 * k + (bR^2)/nSums
                  post_varBR <- post_varBR * k + varBR/nSums
                }
            }
        }
        if ((i%%thin2 == 0) & (i > burnIn)) {
            #tmp <- post_yHat
	    #cat("Inside Burn-in\t",i,"\n")	
            tmp <- post_yHat
            #fileName <- paste(saveAt, "rmYHat", ".dat", sep = "")
            #write(tmp, ncolumns = length(tmp), file = fileName, append = TRUE, sep = " ")
            if (hasRidge) {
                tmp <- post_bR
                #fileName <- paste(saveAt, "rmBR", ".dat", sep = "")
                #write(tmp, ncolumns = length(tmp), file = fileName, append = TRUE, sep = " ")
            }
        }
        tmp <- proc.time()[3]
        ##cat(paste(c("Iter: ", "time/iter: ", "varE: ", "lambda: "), c(i, round(tmp - time, 3), round(varE, 3))))#, round(lambda, 3))))
        ##cat("\n")
        ##cat(paste("------------------------------------------------------------"))
        ##cat("\n")
        time <- tmp
    }
    tmp <- sqrt(post_yHat2 - (post_yHat^2))
    out <- list(y = y, weights = weights, mu = post_mu, varE = post_varE, 
        yHat = I(post_yHat/weights), SD.yHat = I(tmp/weights), 
        whichNa = whichNa)
    names(out$yHat) <- names(y)
    names(out$SD.yHat) <- names(y)
    tmpE <- (yStar - post_yHat)/weights
    tmpSD <- sqrt(post_varE)/weights
    if (nNa > 0) {
        tmpE <- tmpE[-whichNa]
        tmpSD <- tmpSD[-whichNa]
    }
    out$fit <- list()
    out$fit$logLikAtPostMean <- sum(dnorm(tmpE, sd = tmpSD, log = TRUE))
    out$fit$postMeanLogLik <- post_logLik
    out$fit$pD <- -2 * (post_logLik - out$fit$logLikAtPostMean)
    out$fit$DIC <- out$fit$pD - 2 * post_logLik
    if (hasRidge) {
        out$bR <- as.vector(post_bR)
        tmp <- as.vector(sqrt(post_bR2 - (post_bR^2)))
        out$SD.bR <- tmp
        out$varBR <- post_varBR
        names(out$bR) <- namesBR
        names(out$SD.bR) <- namesBR
    }
    out$prior <- prior
    out$nIter <- nIter
    out$burnIn <- burnIn
    out$thin <- thin
    return(out)
}



