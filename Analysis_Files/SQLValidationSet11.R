#args = (commandArgs(TRUE))

#if(length(args)==0){
#    print("No arguments supplied.")
#    stop()
    ##supply default values
    #a = 1
    #b = c(1,1,1)
#}
#else{
#    for(i in 1:length(args)){
#	print(args[[i]])
#    }
#}

#cat(a,"\t",b,"\t",c,"\t",d)

require("Matrix")
dyn.load("/home/aakumar/WORK/PROJECT_1/REGRESSION/sample_betas.so")
#dyn.load("/home/aakumar/WORK_LOCAL/REGRESSION/sample_betas.so")
source("/home/aakumar/WORK/PROJECT_1/REGRESSION/BLR_RIDGE.R")
#require("BLR")
#source("../BLR.R")
#require("snow")
require("RMySQL")
#require("parallel")
#require("reshape2")

#args = commandArgs(TRUE)

mat_func = function(mat,ens_train)
{

  row_mat = rownames(mat)
  ind = row_mat %in% ens_train
  mat_new = mat[ind,ind]

  return(mat_new)
}

extractHGNCMat = function(mat,data_master)
{
   mat1 = mat#mat.or.vec(dim(mat)[1],dim(mat)[2])
   ensg_row = rownames(mat)
   hgnc_row = c()
   hgnc_col = c()
   for(ele in ensg_row)
   {
     hgnc_id = as.vector(data_master[which(as.vector(data_master[,2])==ele),1])
     hgnc_row = c(hgnc_row,hgnc_id)
     hgnc_col = c(hgnc_col,hgnc_id)

   }
   rownames(mat1) = hgnc_row
   colnames(mat1) = hgnc_col

   mat1

}

sortM = function(mat,master_row,master_col)
{
   #sorted.mat = Matrix(0,3000,3000,sparse=TRUE)
   sorted.mat = mat.or.vec(length(master_row),length(master_col))
   row.mat = rownames(mat)
   col.mat = colnames(mat)
   rownames(sorted.mat) = sort(master_row)
   colnames(sorted.mat) = sort(master_col)

   sort.mat.names.row = rownames(sorted.mat)
   sort.mat.names.col = colnames(sorted.mat)
   count =1
   for(ele.r in sort.mat.names.row)
   {
     print(count)
     ind.row = which(row.mat ==ele.r)
     ind.col.list = c()
     
     for(j in 1:length(sort.mat.names.col))
     {
       ele.c = sort.mat.names.col[j]
       ind.col = which(col.mat==ele.c)
       ind.col.list = c(ind.col.list,ind.col)
     }
     val = as.numeric(mat[ind.row,ind.col.list])
     sorted.mat[count,] = val
     count = count+1
   }

   #sorted.mat = sorted.mat+t(sorted.mat)
   #diag(sorted.mat) <- 1
   sorted.mat
}



prep4REG2 = function(matX,matY,ind_train,ind_test,flag,seed_gene=NULL) {

	
	Y.sum = apply(matY,1,sum)

	if (flag==1) {
		ind = which(names(Y.sum)==seed_gene)
		Y.sum[ind] <- NA
		#print(ind)
		#print("inside")
		#Y.sum[which(Y.sum==0)] <- NA
		#Y.sum[ind_test] <- NA
	}
	if (flag==2){
		#Y.sum[ind_test] = NA
		Y.sum = Y.sum[-c(ind_train)]
		ind = which(names(Y.sum)==seed_gene)
		#print(ind)
		#Y.sum[which(Y.sum==0)] <- NA
		Y.sum[ind] <- NA
	
		matX = matX[-c(ind_train),]
		
	}
	#print(length(Y.sum))
	mat_lists = list()
	mat_lists[[1]] = sqrt(Y.sum)
	mat_lists[[2]] = sqrt(matX)
	
	return(mat_lists)
}



prep4REG = function(X_train_mat,Y_train_mat,X_test_mat,Y_test_mat,flag)
{
  #print(dim(Y_test_mat)[1])
  #print(dim(X_test_mat)[1])
  row.X.train = sort(rownames(X_train_mat))#,decreasing=TRUE)
  col.X.train = sort(colnames(X_train_mat))

  row.Y.train = sort(rownames(Y_train_mat))#,decreasing=TRUE)
  col.Y.train = sort(colnames(Y_train_mat))
  
  row.X.test = sort(rownames(X_test_mat))
  #row.Y.test = sort(rownames(X_test_mat))
  col.Y.test = sort(rownames(Y_test_mat))
  
  ind.test = row.X.test %in% col.Y.test
  #val.Y.test = rep(NA,length(col.Y.test))
  
  #names(val.Y.test) = col.Y.test
  
  sortX.train = sortM(X_train_mat,row.X.train,col.X.train)
  sortY.train = sortM(Y_train_mat,row.Y.train,col.Y.train)
  #sortX.test = sortM(X_test_mat,row.X.test) 
  sortX.test = sortM(X_test_mat,rownames(X_test_mat),colnames(X_test_mat))
  sortY.test = sortM(Y_test_mat,rownames(Y_test_mat),colnames(Y_test_mat))

  ##row.names.MAT = unique(c(rownames(sortX.test),rownames(sortY.test)))
  ##col.names.MAT = unique(c(colnames(sortX.test)))

  ##comp.MAT = matrix(0,length(row.names.MAT),length(col.names.MAT),dimnames=list(row.names.MAT,col.names.MAT))

  ##comp.MAT[rownames(sortX.test),colnames(sortX.test)] = comp.MAT[rownames(sortX.test),colnames(sortX.test)]+sortX.test
  ##comp.MAT[rownames(sortY.test),colnames(sortY.test)] = comp.MAT[rownames(sortY.test),colnames(sortY.test)]+sortY.test
  ##comp.MAT = comp.MAT/2

   
  names.sort.Y = rownames(sortY.train)
  ##sortY.train = apply(sortY.train,1, function(x) (x - mean(x))/var(x))
  #Y.sorted.train = apply(sortY.train,1,function(x) sqrt(sum(x^2)))
  #print(sortY.train)
  #Y.sorted.train = apply(sortY.train,1,function(x) sum(x))
  #print(Y.sorted.train)
  Y.sorted.train = apply(sortY.train,1,function(x) sqrt(sum(x^2)))
  ##val.Y.test = apply(sortY.test,1,sum) ####
  #print(val.Y.test)
  ##names.sort.Y1 = names(val.Y.test) ###
  ##ind.test = row.X.test %in% names.sort.Y1 ###
  Y.test = rep(NA,length(row.X.test))
  names(Y.test) = row.X.test
  ##Y.test[ind.test] = val.Y.test ###
  names.sort.X = rownames(sortX.train)
  #print(names.sort.X)
  ind = which(names.sort.X %in% names.sort.Y==FALSE)
  #ind.test = whi
  ##print(ind)
  ##print(names.sort.Y)
  ##print(names.sort.X)
  #print(ind)
  row.Y = names.sort.Y
  
  for(i in ind)
  {
    print("Yes")
    print(ind)
    Y.sorted.train = append(Y.sorted.train,0,after=i-1)
    row.Y = append(row.Y,names.sort.X[i],after=i-1)
  }

  names(Y.sorted.train) <- row.Y
  Y.sorted.train = c(Y.sorted.train,Y.test)
  if(flag==1)
  {
    sortX = rbind(sortX.train,comp.MAT)
  }
  else
  {
    sortX = rbind(sortX.train,sortX.test)

  }
  mat_lists = list()
  ##print(Y.sorted.train)
  mat_lists[[1]] <- sqrt(Y.sorted.train)#Y.sorted[-ind]
  #mat_lists[[1]] <- sort(Y.sorted.train)#Y.sorted[-ind]
  #mat_lists[[1]] <- 1/(Y.sorted.train)#Y.sorted[-ind]
  #mat_lists[[1]] <- Y.sorted[-ind]
  mat_lists[[2]] <- sqrt(sortX)#sortX[-ind,-ind]
  #mat_lists[[2]] <- sortX#sortX[-ind,-ind]
  #mat_lists[[2]] <- sortX[-ind,-ind]

  return (mat_lists)

}

getNames = function(data_train,y_sort)
{
  ens_train = as.vector(data_train[,2])
  master_name = c()
  #master_val = c()
  for(ele in y_sort)
  {
    #print(ele)
    ind = which(ens_train==ele)
    if(length(ind)>1)
    {
      
      name_hgnc = as.vector(data_train[ind,1])
      #print(name_hgnc)
      name_hgnc = paste(name_hgnc[1],name_hgnc[2],sep="|")
    }
    else
    {
      name_hgnc = as.vector(data_train[ind,1])
    }
    master_name = c(master_name,name_hgnc)
    #master_val = c(master_val,
  }
  return(master_name)
}

execTrainTest <-function(train_file,test_file,flag)
{
  #print(train_file)
  #print(test_file)
  #data_train = read.table("TRAIN_TEST/Train_set.txt",sep="\t")
  data_train = read.table(train_file,sep="\t")
  data_train[,1] = gsub(" ","",data_train[,1])
  #data_test = read.table("TRAIN_TEST/Priority_list_MAAS.txt",sep="\t")
  data_test = read.table(test_file,sep="\t")
  data_test[,1] = gsub(" ","",data_test[,1])
  #data_master = read.table("TRAIN_TEST/MASTER_ENS_UNIQUE_20K.txt",sep="\t")
  data_master = read.table("MASTER_ENS_UNIQUE_20K.txt",sep="\t")
  data_master[,1] = gsub(" ","",data_master[,1])
  data_master[,2] = gsub(" ","",data_master[,2])
  master_ind = which(as.vector(data_master[,1]) %in% as.vector(data_test[,1])) 
  ens_train = c()
  ens_test = c()
  for(ele in as.vector(data_test[,1]))
  {
    ind_test = which(as.vector(data_master[,1])==ele)
    #ind_test = which(as.vector(data_master[,1])==ele)
    if(length(ind_test)>1 | length(ind_test)==0)
    {
      cat(ele,"\n",file="../R_DATA/NON_ID.txt",append=TRUE)
    }
    else{
    ens_test = c(ens_test,as.vector(data_master[ind_test,2]))
    }

  }
  for(ele in as.vector(data_train[,1]))
  {
    ind_train = which(tolower(as.vector(data_master[,1]))==tolower(ele))
    cat(ele,"\t",ind_train,"\n",file="../R_DATA/Double_ind_train_2.txt",append=TRUE)
    if(length(ind_train)>1 | length(ind_train)==0)
    {
      cat(ele,"\n",file="../R_DATA/Double_ind_train.txt",append=TRUE)
    }
    else
    {
      ens_train = c(ens_train,as.vector(data_master[ind_train,2]))
    }
  }
  ens_train = unique(ens_train)
  ens_test = unique(ens_test)

  ens_list = list()
  ens_list[[1]] <- ens_train
  ens_list[[2]] <- ens_test

  return(ens_list)

}
 
creatMatList <- function(mat_train_X,mat_train_Y,mat_test_X,mat_test_Y,flag) {
  
  #mat_list = prep4REG(log(mat_train_X),log(mat_train_Y),log(mat_test_X),log(mat_test_Y),flag)
  mat_list = prep4REG(mat_train_X,mat_train_Y,mat_test_X,mat_test_Y,flag)
  #mat_list = prep4REG2(mat_train_X, mat_train_Y,mat_test_X,mat_test_Y)
 # mat_list = list()
  ##mat_list[[1]] = mat_train_X
  ##mat_list[[2]] = mat_train_Y
  ##mat_list[[3]] = mat_test_X
  ##mat_list[[4]] = mat_test_Y
  mat_list 

}




execHGNCName2 = function(fm,X,ind,data_master)
{
	#print(dim(X))
	y.pred = fm$yHat[ind:dim(X)[1]]
	#y.pred = fm$yHat[ind_test]
	#x.pred = rownames(X)[ind_test]
	x.pred = rownames(X)[ind:dim(X)[1]]
	pred.frame = data.frame(cbind(x.pred,y.pred))
	sorted.pred.frame = pred.frame[order(pred.frame$y.pred,decreasing=TRUE),]
	hgnc.names = getNames(data_master,as.vector(sorted.pred.frame$x.pred))
	hgnc_list = list()
	hgnc_list[[1]] <- hgnc.names
	hgnc_list[[2]] <- as.vector(sorted.pred.frame[,2])
	hgnc_list
}


#myFunc = function(ensg_query,ensg_target,con) {
getMySQLMat = function(query_ind_list,q_t_flag) {

	require("RMySQL")	
	ensg_query = query_ind_list[[1]]
	ensg_target = query_ind_list[[2]]
	#master_ensg = unique(c(ensg_query,ensg_target))
	print(length(ensg_query))
	print(length(ensg_target))
	master_ensg = ensg_query
	mat.tmp = mat.or.vec(length(master_ensg),length(master_ensg))
	rownames(mat.tmp) = master_ensg
	colnames(mat.tmp) = master_ensg

	master_ensg_list = c()	
	target_list = c()
	for(ele in master_ensg) {
		tmp = paste("'",ele,"'",sep="")
		master_ensg_list = c(master_ensg_list,tmp)
	}
	master_ensg_list = paste(master_ensg_list,sep="",collapse=", ")
	head(master_ensg_list)
	
	con <- dbConnect(MySQL(),user='aakumar',password='aakumar',dbname='aakumar',host='143.169.238.18')
	if (q_t_flag=="X") {
		statement = paste("SELECT * FROM COMPOSITE_X11 where ENSG_ROW IN (",master_ensg_list,") AND ENSG_COL IN (",master_ensg_list,")",sep="")

	}
	else {

		statement = paste("SELECT * FROM COMPOSITE_Y11 where ENSG_ROW IN (",master_ensg_list,") AND ENSG_COL IN (",master_ensg_list,")",sep="")
	}

	results = dbSendQuery(con,statement)
	vals = dbFetch(results,n=-1)
	for ( i in 1:dim(vals)[1]) {

		row_ind = which(master_ensg==vals[i,1])
		col_ind = which(master_ensg==vals[i,2])
		#print(col_ind)
		score = vals[i,3]
		mat.tmp[row_ind,col_ind] = score
		mat.tmp[col_ind,row_ind] = score
	}

	rowIndX = master_ensg %in% ensg_query
	colIndX = master_ensg %in% ensg_target
	ind_col = which(which(colIndX==TRUE) <=length(ensg_target))
	print(ind_col)
	#matX = as.matrix(mat.tmp[rowIndX,colIndX])
	matX = as.matrix(mat.tmp[rowIndX,ind_col])
	dbClearResult(dbListResults(con)[[1]])
	dbDisconnect(con)	
	return(matX)	
}



#data_master = read.table("MASTER_ENS_UNIQUE_20K.txt",sep="\t")
#data_master[,1] = gsub(" ","",data_master[,1])
#data_master[,2] = gsub(" ","",data_master[,2])

execPrior = function(strs_train, strs_test, seed_gene,out_dir,flag) {

	data_master = read.table("MASTER_ENS_UNIQUE_20K.txt",sep="\t")
	data_master[,1] = gsub(" ","",data_master[,1])
	data_master[,2] = gsub(" ","",data_master[,2])

	#gene= gsub(" ","",as.vector(read.table(seed_gene)[,1]))
	gene = seed_gene
	
	#print(gene)
	ensg_seed = data_master[which(data_master[,1]==gene),2]
	#print(strs_train)
	#print(strs_test)

	ens_list = execTrainTest(strs_train,strs_test,2)

	print(ens_list[[1]])
	print(ens_list[[2]])

	ens_train = ens_list[[1]]
	ens_test = ens_list[[2]]

	rowEnsg = c(ens_train,ens_test)
	colEnsg = ens_train

	#cat("col Ensg\n")
	#print(colEnsg)	
	ind_test = which(rowEnsg %in% ens_test == TRUE)
	ind_train = which(rowEnsg %in% ens_train == TRUE)

	col_ind_train = which((ind_train <= length(colEnsg)) ==TRUE)
	ind_train = col_ind_train

	data_test = read.table(strs_test)
	data_train = read.table(strs_train)

	print(rowEnsg)
	print(colEnsg)	
	
	query_list = list()	
	query_list[[1]] = rowEnsg
	query_list[[2]] = colEnsg

	#print(is.null(colEnsg))	
	a1 = TRUE

	if (is.null(rowEnsg) || is.null(colEnsg)) {
		a1 = FALSE

	}

	if (a1) {
	#matX = getMySQLMat(query_list,"X")
	matX = getMySQLMat(query_list,"X")#[,1:5]
	matY = getMySQLMat(query_list,"Y")#[,1:5]

	print(dim(matX))
	print(dim(matY))	

	a1 = TRUE
	#if (is.null(dim(matX)) && is.null(dim(matY))) {
	#	print("inside")
	#	a1 = FALSE
	#}
	#if (a1) {
	#print("inside a1")
	mat_list = prep4REG2(matX,matY,ind_train,ind_test,flag,ensg_seed)

##################3
# REGRESSION
#
	data_master = read.table("MASTER_ENS_UNIQUE_20K.txt",sep="\t")
	data_master[,1] = gsub(" ","",data_master[,1])
	data_master[,2] = gsub(" ","",data_master[,2])

	yNa = mat_list[[1]]
	X = mat_list[[2]]

	#stop()
	#print(length(yNa))
	whichNa = as.vector(which(is.na(yNa)==TRUE))
	indNonNa = as.vector(which(is.na(yNa)==FALSE))
	diff = length(yNa) - length(ind_train)

	if (diff <0) {
		df_var = 3
	}
	else {
		df_var = length(yNa[indNonNa]) - 2#3#diff -2
	}
	
	S_var = var(yNa[indNonNa])/2*df_var
        h2 = 0.50
        Vy = var(yNa[indNonNa])
        S_var =h2*var(yNa[indNonNa])*df_var
	#print(X[indNonNa,])
        MSx <- sum(apply(FUN=var,MARGIN=2,X=as.matrix(X[indNonNa,])))
        Sr <- Vy*h2*(df_var)/MSx
	
		
	fm<-BLR_new(y=yNa,XR=X, prior=list(varE=list(df=df_var,S=S_var),varBR = list(df=df_var,S=Sr),lambda=list(shape=1.2,rate=1e-4,type='random',value=30)),nIter=100000,burnIn=30000,thin=10,saveAt="../R_DATA/example1_")
	ind = c()
	print(ind_train)
	if (flag==1) {
		ind = length(ind_train)+1
	}
	else {
		ind = 1
	}
	a1 = fm$y
	a2 = fm$yHat

	#print(fm$yHat[whichNa])

	#print(yNa[whichNa])
	#b.tst = mean((fm$yHat[whichNa]-yNa[whichNa])^2)
	b.trn = mean((fm$yHat[indNonNa]-yNa[indNonNa])^2)
	print(b.trn)	
	
	hgnc_list = execHGNCName2(fm,X,ind,data_master)

	pred.frame = as.data.frame(cbind(hgnc_list[[1]],hgnc_list[[2]]))
	write.table(pred.frame,sep="\t",file="output_hgnc_list.txt",row.names=T,col.names=F)

	obj = list()
	obj[[1]] = hgnc_list
	obj[[2]] = yNa
	obj[[3]] = X

	return(obj)
}
}


#Dis_list = c("CARDIOVASCULAR", "DERMATOLOGICAL","ENDOCRINE","IMMUNOLOGICAL","MUSCULAR","RENAL","CONNECTIVE_TISSUE","DEVELOPMENT","HEMATOLOGICAL","METABOLIC","OPTHAMALOGICAL","SKELETAL")
#Dis_list = c("DEVELOPMENT","HEMATOLOGICAL","METABOLIC","OPHTHAMOLOGICAL","SKELETAL")
#Dis_list = c("OPTHAMALOGICAL","SKELETAL")
#Dis_list = c("CARDIOVASCULAR")

execDisease <- function() {
	#Dis_list = c("DEVELOPMENT")
			Tr_path_file = "/home/aakumar/WORK/PROJECT_1/VALIDATION/MARIKE/Train_set.txt"
			#Ts_path_file = "/home/aakumar/WORK/PROJECT_1/VALIDATION/MARIKE/Test_set.txt"
			Ts_path_file = "/home/aakumar/WORK/PROJECT_1/VALIDATION/MARIKE/Test_set_Final.txt"
			rank_dir = "/home/aakumar/WORK/PROJECT_1/VALIDATION/MARIKE/"
			#Tr_path_file = "/home/aakumar/WORK/PROJECT_1/VALIDATION/12_SET_RUN6/CARDIOVASCULAR/TRAIN/Train_31.txt"
			#Ts_path_file = "/home/aakumar/WORK/PROJECT_1/VALIDATION/12_SET_RUN6/CARDIOVASCULAR/TEST/Test_31.txt"
			#Ts_path_file = "/home/aakumar/WORK/PROJECT_1/VALIDATION/DISEASE_ASS/RUN_2/umls:C3160718/TEST/Test_12.txt"
			seed_gene="PARK12"
			#cat(ele,"\t",seed_gene,"\n")
			obj = execPrior(Tr_path_file,Ts_path_file,seed_gene,rank_dir,1)
			#stop()
			#print(seed_gene)
}

obj = execDisease()
