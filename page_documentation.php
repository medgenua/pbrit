<?php
if (isset($_GET['t'])) {
	$topic = $_GET['t'];
}
else {
	$topic = 'overview';
}
if ($topic != 'overview') {
	echo "<div class=section>";
	echo "<h3>Documentation: ".str_replace("_"," ",$topic)."</h3>";
	if (file_exists("includes/inc_doc_$topic.inc")) {
		include("includes/inc_doc_$topic.inc");
	}
	else {
		echo "<p class=todo>Documentation missing</p>";
		echo "<p><a href='index.php?page=documentation'>Back to overview</a></p>";
		echo "</div>";
	}
}
else {
	echo "<div class=section>";
	echo "<h3>Documentation</h3>";
	echo "<p><ol>";
	echo "<li><a href='index.php?page=documentation&t=Introduction'>Introduction to pBRIT methodology</a></li>";
	echo "<li><a href='pBRIT_Intro_files/Run_pBRIT.pdf'>How to run pBRIT</a></li>";
	echo "<li><a href='pBRIT_Intro_files/Explain_Result.pdf'> How to Interpret the Results</a></li>";
	echo "<li><a href='index.php?page=documentation&t=Batch_Processing'>How use pBRIT in batch mode</a></li>";
	//echo "<li><a href='index.php?page=documentation&t=Citing_pBRIT'>How should I cite pBRIT</a></li>";
	echo "</ol></p>";
}

		
