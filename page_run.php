<div class=section>

<h3>Prioritize Gene Lists</h3>

<p> pBRIT prioritizes candidate genes based on a set of training genes provided by the user. pBRIT incorporates two information-theoretic approaches for data fusion:  </p>
	<ul style="list-style-type:circle">
		<li> <b>TFIDF</b> </li>
		<li> <b>TFIDF_SVD</b> </li>
	</ul>

<p> Under each of the data fusion methods, there are two alternative regression designs:  </p>
<ul style="list-style-type:circle">
	<li> <b> Test.Pheno.Include </b> : Phenotypic information of the candidate genes is included in the regression model. </li>
	<li> <b> Test.Pheno.Discard </b> : Phenotypic information of the candidate genes is not included in the regression model. </li>
</ul>

<p> 
	In our cross-validation studies, <b>TFIDF_SVD </b> combined with <b>Test.Pheno.Include </b> gave comparatively better results.

</p>
<br><br> 
<form action='index.php?page=submit_job' method=POST>
<p><span class=emph>Set Run Parameters</span></p> 
<p class=indent><input type=text name=jobname id=jobname size=40 /> : Provide a name to identify your prioritization job. </p>
<p class=indent><input type=text name=notify id=notify size=40 /> : Optionally provide your email to recieve a notification when the prioritization is finished.</p>

<p class=indent><select style='width:10em;' name=method id=method><option selected=selected value='TFIDF_SVD'>TFIDF_SVD</option><option value='TFIDF'>TFIDF</option></select> : pBRIT datafusion method</p>
<p class=indent><select style='width:10em;' name='use_pheno' id='use_pheno'><option selected value='1'>Include</option><option  value='2'>Discard</option><select>: Include known test-gene phenotype annotations into the regression model. Discarding the phenotype information forces pBRIT to purley predict the phenotype association on the functional annotations.</p>
<?php 
$q = mysql_query("SELECT prefix FROM `Annotation_Releases` ORDER BY time_added DESC");
echo "<p class=indent><select style='width:10em;' name='db_version' id='db_version'>";
while ($row = mysql_fetch_array($q)) {
	echo "<option selected value='".$row[0]."'>$row[0]</option>";
}
echo "</select> : Database Release. The publication was based on release January 2015.</p>";
?>
<!-- comment this out to disable the beta code -->
<?php
// some comment on how beta differs, edit if needed
//$title = 'Current beta code is aimed at performance of pre-processing. No changes were made to the prioritization itself.';
//echo "<p class=indent><input type=checkbox title='$title' name='beta' value='1'> Use beta code</p>";
?>
<!-- End of BETA activation black -->


<p>
	<table  cellspacing=0 width='100%'>
		<tr><th >Training Genes</th><th >Test Genes</th></tr>
		<tr>
			<td>A set of genes known to be related to the phenotype of the patient. These genes will be used to train the model. Please provide at least "3" Training genes.</td>
			<td>A set of genes affected in the patient. These genes will be ranked on their similarity to the training set.</td>
		</tr>

		<tr>
			<td>Required format: GeneIDs according to HUGO, one per line.</td>
			<td>Required format: GeneIDs according to HUGO, one per line.</td>
		</tr>
		<tr>
		  <td ><textarea id=trainingset name=trainingset cols=70 rows=25 ></textarea></td>
		  <td><textarea id=testset name=testset cols=70 rows=25 ></textarea></td>
		</tr>
		<tr>
			<td colspan=2 class=last>&nbsp;</td>
		</tr>
	</table>
</p>
<p class=indent><input class=required id=email name=email value='Provide your email' size=40 /> <input type=submit class=button name=submit id=submit value='Submit'/>
</p>
</form>
</p>
