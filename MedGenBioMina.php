<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href='http://medgen.ua.ac.be/~gvandeweyer/' />
<title>Transfer Testing Results</title>
<style type='text/css'>
.main {
	background-color:#cdcdcd;
}
.sub {
	background-color:#efefef;
}
th {
	border:solid 1px #aeaeae;
	padding:3px;
	padding-left:10px;
	padding-right:10px;
}
td {
	border:solid 1px #aeaeae;
	padding:3px;
	padding-left:10px;
	padding-right:10px;
}	
</style>
</head>
<body>
<?php
// variables? 
if (isset($_GET['from'])) {
	$from = $_GET['from'];
}
else {
	$from = 0;
}
if (isset($_GET['limit'])) {
	$limit = $_GET['limit'];
}
else{
	$limit = 30;
}

#############################
## CONNECT TO THE DATABASE ##
#############################
include('/opt/ServerMaintenance/PHPcredentials.inc');

mysql_select_db('gvandeweyer');

?>
<h1>Data Transfer stability results</h1>
<p>Results are ordered by date in descending mode. Some overviews are shown on top. <br/>Checksums were generated using MD5, transfers performed by scp. <br/>Two parallel transfers are started per iteration, each form and to seperate harddisks. <br/>The files are deleted after every transfer, so needed diskspace is limited to the actual file size.</p>
<p>Transfers are active after office hours (18u-7u), and during weekends.</p>
<?php echo "<p><a href='MedGenBioMina.php?limit=$limit&from=$from'>Click here to refresh</a></p>\n"; ?>
<hr/>
<h2>Result summaries</h2>
<?php
$query = mysql_query("SELECT SUM(avgup) AS up, SUM(avgdown) AS down, COUNT(id) AS nr, SUM(successrate) AS sr FROM TransferTesting WHERE avgup > 0");
$row = mysql_fetch_array($query);
$nr = $row['nr'];
$sr = $row['sr'];
if ($nr > 0) {
	$avgup = number_format(($row['up'] / $nr),1,'.',',');
	$avgdown = number_format(($row['down'] / $nr),1,'.',',');
}
else {
	$avgup = 0;
	$avgdown = 0;
}
echo "<p><ul>";
echo "<li>Number of datapoints: $nr</li>";
echo "<li>Average Medgen => Biomina speed: $avgup"."Mb/s for each file</li>";
echo "<li>Average Biomina => Medgen speed: $avgdown"."Mb/s for each file</li>";
echo "<li>Successrate : $sr/". ($nr * 2) ."</li>";
echo "</ul></p>";
?>
<hr/>
<h2>Detailed results</h2>

<?php
# links
echo "<p>";
if ($from > 0) {
	$newfrom = $from - $limit;
	if ($newfrom < 0) {
		$newfrom = 0;
	}
	else {
		echo "<a href='MedGenBioMina.php?limit=$limit&from=0'>Most Recent Results</a> | ";
	}	
	echo "<a href='MedGenBioMina.php?limit=$limit&from=$newfrom'>More Recent Results</a> | ";
}
$newfrom = $from + $limit;
echo "<a href='MedGenBioMina.php?limit=$limit&from=$newfrom'>Older Results</a></p>";


# get data
$query = mysql_query("SELECT content FROM TransferTesting ORDER BY id DESC LIMIT $from,$limit");
echo "<p><table cellspacing=0>";
while ($row = mysql_fetch_array($query)) {
	# correct degree symbol...
	 #$row['content'] = htmlentities($row['content']);
	echo preg_replace('/(.*\d)(.*)(C,.*)/','$1&deg;$3',$row['content']);
}
echo "</table></p>";
?>
</body>
</html>
