<?php
require("../includes/LoadCredentials.php");

if (!isset($_GET['p']) || !file_exists("$scriptdir/".$_GET['p'])) {
	echo "<p>invalid_path</p>";
	exit;
}

$job_path = $_GET['p'];
//$gene = $_GET['g'];
// if heatmap does not exist, create it.
if (!file_exists("$scriptdir/$job_path/model_betas.png")) {
	// get db_version
	system("/opt/software/R/3.2.1/bin/Rscript '$scriptdir/Analysis_Files/parse_pBRIT.object.Rscript' '$scriptdir/$job_path/' >/dev/null 2>&1");
}

if (!file_exists("$scriptdir/$job_path/model_betas.png")) {
	echo "<p>ERROR: Could not create the requested plots</p>";
}
else {
	echo "<p><span  class=emph>Regression Parameters:</span></p><p><a href='$job_path/model_betas.png' rel=lightbox><img src='$job_path/model_betas.png' width='98%' rel=lightbox title='click to enlarge'/></a></p>";
	echo "<p><span class=emph>Download Full Model</span>:</p><p>This file is presented as an R-object: <a href='$job_path/pBRIT_fm_object.dat'>Right-Click and Save-As</a></p>";
}
?>
