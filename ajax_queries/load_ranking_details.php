<?php
require("../includes/LoadCredentials.php");

// input.
$test = $_GET['test'];
$train = $_GET['train'];
$anno = $_GET['anno'];
$path = $_GET['path'];
$db_version = $_GET['dbv'];

$table = $db_version."_$anno"."_TFIDF";

// Get annotations for training.

$query = mysql_query("SELECT `feature`,`tfidf` FROM  `$table` WHERE `ensgid` = '$train'");
$train_anno = array();
while ($row = mysql_fetch_array($query)) {
	$train_anno[$row['feature']] = $row['tfidf'];
}
error_log(implode("; ",array_keys($train_anno)));
// get top 100 annotations for test in descending order.
$output = '';
if ($anno == 'GO') {
	$names = $db_version."_$anno"."_namespace";

	$query = mysql_query("SELECT gon.`feature`, go.`feature` AS goid, go.`tfidf` FROM `$table` go JOIN `$names` gon ON go.feature = gon.goid WHERE go.`ensgid` = '$test' ORDER BY go.tfidf DESC ");
	//$query = mysql_query("SELECT go.`feature`, go.`feature` AS goid, go.`tfidf` FROM `$table` go WHERE go.`ensgid` = '$test' ORDER BY go.tfidf DESC ");

	$idx = $skip = 0;
	while ($row = mysql_fetch_array($query)) {
		$idx++;
		if ($idx == 101) {
			$output .=  "<tr><th class=topsmall colspan=4>Summary of low scoring matches</th></tr>";
			$skip = 1;
		}
		$hit = 0;
		$line = "<tr><td>$idx</td><td><a href='http://amigo.geneontology.org/amigo/term/GO:".$row['goid']."' target='_blank'>".$row['feature']."</a></td><td>".number_format($row['tfidf'],4)."</td>";
		if (array_key_exists($row['goid'],$train_anno)) {
			$line .= "<td>".number_format($train_anno[$row['goid']],4)."</td></tr>";
			$hit = 1;
		}
		else {
			$line .= "<td> -- </td></tr>";
		}
		if ($hit == 1 || $skip == 0) {
			$output .= $line;
		}
	}
}
elseif ($anno == 'HP') {
	$names = $db_version."_$anno"."_namespace";
	$query = mysql_query("SELECT hpn.`feature`, hp.`feature` AS hpid, hp.`tfidf` FROM `$table` hp JOIN `$names` hpn ON hp.feature = hpn.hpid WHERE hp.`ensgid` = '$test' ORDER BY hp.tfidf DESC ");
	//$query = mysql_query("SELECT `feature`,`tfidf` FROM `$table` WHERE `ensgid` = '$test' ORDER BY tfidf DESC ");
	$idx = $skip = 0;
	while ($row = mysql_fetch_array($query)) {
		$idx++;
		
		if ($idx == 101) {
			$output .=  "<tr><th class=topsmall colspan=4>Summary of low scoring matches</th></tr>";
			$skip = 1;
		}
		$line = "<tr><td>$idx</td><td><a href='http://compbio.charite.de/hpoweb/showterm?id=HP:".$row['hpid']."' target='_blank'>".$row['feature']."</a></td><td>".number_format($row['tfidf'],4)."</td>";
		$hit = 0;
		if (array_key_exists($row['hpid'],$train_anno)) {
			$line .= "<td>".number_format($train_anno[$row['hpid']],4)."</td></tr>";
			$hit = 1;
		}
		else {
			$line .= "<td> -- </td></tr>";
		}
		if ($skip == 0 || $hit == 1) {
			$output .= $line;
		}
	}


}
elseif ($anno == 'MP') {
	$names = $db_version."_$anno"."_namespace";
	$query = mysql_query("SELECT hpn.`feature`, hp.`feature` AS hpid, hp.`tfidf` FROM `$table` hp JOIN `$names` hpn ON hp.feature = hpn.hpid WHERE hp.`ensgid` = '$test' ORDER BY hp.tfidf DESC ");
	//$query = mysql_query("SELECT `feature`,`tfidf` FROM `$table` WHERE `ensgid` = '$test' ORDER BY tfidf DESC ");
	$idx = $skip = 0;
	while ($row = mysql_fetch_array($query)) {
		$idx++;
		
		if ($idx == 101) {
			$output .=  "<tr><th class=topsmall colspan=4>Summary of low scoring matches</th></tr>";
			$skip = 1;
		}
		$line = "<tr><td>$idx</td><td><a href='http://www.ebi.ac.uk/ols/ontologies/mp/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FMP_".$row['hpid']."' target='_blank'>".$row['feature']."</a></td><td>".number_format($row['tfidf'],4)."</td>";
		$hit = 0;
		if (array_key_exists($row['hpid'],$train_anno)) {
			$line .= "<td>".number_format($train_anno[$row['hpid']],4)."</td></tr>";
			$hit = 1;
		}
		else {
			$line .= "<td> -- </td></tr>";
		}
		if ($skip == 0 || $hit == 1) {
			$output .= $line;
		}
	}


}

elseif ($anno == 'DP'){
	$names = $db_version."_$anno"."_namespace";

	$query = mysql_query("SELECT dpn.`feature`, dp.`feature` AS dpid, dp.`tfidf` FROM `$table` dp JOIN `$names` dpn ON dp.feature = dpn.doid WHERE dp.`ensgid` = '$test' ORDER BY dp.tfidf DESC ");
	$idx = $skip = 0;
	while ($row = mysql_fetch_array($query)) {
		$idx++;
		
		if ($idx == 101) {
			$output .=  "<tr><th class=topsmall colspan=4>Summary of low scoring matches</th></tr>";
			$skip = 1;
		}
		$line = "<tr><td>$idx</td><td><a href='http://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_".$row['dpid']."' target='_blank'>".$row['feature']."</a></td><td>".number_format($row['tfidf'],4)."</td>";
		$hit = 0;
		if (array_key_exists($row['dpid'],$train_anno)) {
			$line .= "<td>".number_format($train_anno[$row['dpid']],4)."</td></tr>";
			$hit = 1;
		}
		else {
			$line .= "<td> -- </td></tr>";
		}
		if ($skip == 0 || $hit == 1) {
			$output .= $line;
		}
	}
}
elseif ($anno == 'HD'){
	$names = $db_version."_$anno"."_namespace";

	$query = mysql_query("SELECT hgn.`name`, hgn.`hgid` AS hgid, hg.feature, hg.`tfidf` FROM `$table` hg JOIN `$names` hgn ON hg.feature = hgn.feature WHERE hg.`ensgid` = '$test' ORDER BY hg.tfidf DESC ");
	$idx = $skip = 0;
	while ($row = mysql_fetch_array($query)) {
		$idx++;
		
		if ($idx == 101) {
			$output .=  "<tr><th class=topsmall colspan=4>Summary of low scoring matches</th></tr>";
			$skip = 1;
		}
		$line = "<tr><td>$idx</td><td><a href='https://phgkb.cdc.gov/HuGENavigator/phenoPedia.do?firstQuery=".$row['name']."&cuiID=".$row['hgid']."&typeSubmit=GO&check=y&which=2&pubOrderType=pubD' target='_blank'>".$row['name']." (".$row['hgid'].")</a></td><td>".number_format($row['tfidf'],4)."</td>";
		$hit = 0;
		if (array_key_exists($row['feature'],$train_anno)) {
			$line .= "<td>".number_format($train_anno[$row['feature']],4)."</td></tr>";
			$hit = 1;
		}
		else {
			$line .= "<td> -- </td></tr>";
		}
		if ($skip == 0 || $hit == 1) {
			$output .= $line;
		}
	}
}
elseif ($anno == 'PB') {
	// get symbols.
	$url = 'https://www.ncbi.nlm.nih.gov/pubmed/?term=(';
	$query = mysql_query("SELECT `HGNC_NAME` FROM `$db_version"."_HGNC_ENSG_MAP` WHERE `ENSG_ID` = '$train'");
	while ($row = mysql_fetch_array($query)) {
		$url .= $row[0]."[Title/Abstract] OR ";
	}
	$url = substr($url,0,-4).") AND (";
	$query = mysql_query("SELECT `HGNC_NAME` FROM `$db_version"."_HGNC_ENSG_MAP` WHERE `ENSG_ID` = '$test'");
	while ($row = mysql_fetch_array($query)) {
		$url .= $row[0]."[Title/Abstract] OR ";
	}
	$url = substr($url,0,-4).")";

	$output = "<p>Pubmed text-mining can not be easily visualized by overlapping features. As a starting point, you can search pumbed for common publications: <a href='$url' target='_blank'>Here</a></p>";

}
elseif ($anno == 'BL') {
	// get symbols.
	$table = $db_version."_$anno"."_SIM_MAT";
	$query = mysql_query("SELECT `SCORE` FROM `$table` WHERE `ENSG_ROW` IN ('$test','$train') AND `ENSG_COL` IN ('$test','$train') AND `ENSG_ROW` != `ENSG_COL`");
	$row = mysql_fetch_array($query); 

	$output = "<p>Sequence similarity scores reflect a normalized E-value from protein-protein BLAST.</p><p>The score is '".$row['SCORE']."'</p>";

}

elseif ($anno != 'GO') {
	$query = mysql_query("SELECT `feature`,`tfidf` FROM `$table` WHERE `ensgid` = '$test' ORDER BY tfidf DESC ");
	$idx = $skip = 0;
	while ($row = mysql_fetch_array($query)) {
		$idx++;
		
		if ($idx == 101) {
			$output .=  "<tr><th class=topsmall colspan=4>Summary of low scoring matches</th></tr>";
			$skip = 1;
		}
		$line = "<tr><td>$idx</td><td>".$row['feature']."</td><td>".number_format($row['tfidf'],4)."</td>";
		$hit = 0;
		if (array_key_exists($row['feature'],$train_anno)) {
			$line .= "<td>".number_format($train_anno[$row['feature']],4)."</td></tr>";
			$hit = 1;
		}
		else {
			$line .= "<td> -- </td></tr>";
		}
		if ($skip == 0 || $hit == 1) {
			$output .= $line;
		}
	}
}

if ($output != '' && $anno != 'PB' && $anno != 'BL') {
	echo "<p><table width='100%' cellspacing=0>";
	echo "<tr><th class=topsmall NOWRAP>RANK</th><th class=topsmall NOWRAP>Feature</th><th class=topsmall NOWRAP>Test Score</th><th class=topsmall NOWRAP>Training Score</th></tr>";
	echo "$output";
	echo "<tr><td class=last colspan=4>&nbsp;</td></tr></table></p>";
}
elseif ($anno == 'PB' || $anno == 'BL') {
	echo $output;
}
else {
	echo "<p>No features found for the selected test gene in $anno.</p>";
}
?>
