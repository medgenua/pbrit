<?php
require("../includes/LoadCredentials.php");

if (!isset($_GET['p']) || !file_exists("$scriptdir/".$_GET['p'])) {
	echo "<p>invalid_path</p>";
	exit;
}

if (!isset($_GET['g']) ) {
	echo "<p>no gene provided</p>";
	exit;
}

$job_path = $_GET['p'];
$gene = $_GET['g'];
if (!file_exists("$scriptdir/$job_path/gene_heatmaps")) {
	mkdir("$scriptdir/$job_path/gene_heatmaps");
}
// if heatmap does not exist, create it.
if (!file_exists("$scriptdir/$job_path/gene_heatmaps/$gene.png")) {
	// get db_version
	$db_version = rtrim(file_get_contents("$scriptdir/$job_path/db_version"));
	$method = rtrim(file_get_contents("$scriptdir/$job_path/method"));
	system("/opt/software/R/3.2.1/bin/Rscript '$scriptdir/Analysis_Files/extractAnnotationMatrix.R' '$gene' '$scriptdir/$job_path/' '$method' '$db_version'");
}

if (!file_exists("$scriptdir/$job_path/gene_heatmaps/$gene.png")) {
	echo "ERROR: plot could not be created\n";
}
else {
	echo "$job_path/gene_heatmaps/$gene.png";
}
?>
