<?php

if (!isset($_GET['p'])) {
	echo "<p>Select an iteration to show details</p>";
	exit;
}

$seed = str_replace("REPLACE_THIS","SEED/Seed",$_GET['p']);
$test = str_replace("REPLACE_THIS","TEST/Test",$_GET['p']);
$train = str_replace("REPLACE_THIS","TRAIN/Train",$_GET['p']);

if (!file_exists("../$seed")) {
	echo "<p>Invalid iteration provided</p>";
	exit;
}

// print details
$seed_gene = rtrim(file_get_contents("../$seed"));
$train_genes = explode("\n",rtrim(file_get_contents("../$train")));
$test_genes =  explode("\n",rtrim(file_get_contents("../$test")));

echo "<p><ul class=disc>";
echo "<li>Seed Gene: $seed_gene</li>";
echo "<li>Train Genes <span class=italic>(n=".count($train_genes).")</span>: ".implode(", ",$train_genes)."</li>";
echo "<li>Test Genes <span class=italic>(n=".count($test_genes).")</span>: ".implode(", ",$test_genes)."</li>";
//echo "</ul></p>";

echo "<li>Ranking Output<br/>";
// rank table
echo "<span style='width:47%;margin-right:3%;float:left;position:relative;'>";
echo "<table width='100%' cellspacing=0 style='margin-top:1em;'>";
echo "<tr><th>Rank</th><th>Rank-Ratio</th><th>Gene</th><th>Score</th><th>Explore</th></tr>";

$path = $_GET['p'];
$path = preg_replace("/REPLACE_THIS.*/","",$path);

$method = 'TFIDF';
error_log($path);
if (preg_match("/TFIDF_SVD/",$path)) {
	$method = 'SVD';
}
error_log($method);
$comm = "ls ../$path/OUTPUT/$seed_gene"."_output*txt";
$outfile = rtrim(`$comm`);
$nrlines=rtrim(`cat '$outfile' | wc -l`);
$fh = fopen("$outfile",'r');
while (($line = fgets($fh)) !== false) {
		$c = explode("\t",str_replace('"','',rtrim($line)));
		echo "<tr><td>$c[0]</td><td>".round(100*$c[0]/$nrlines,2)."%</td><td>$c[1]</td><td>".round($c[2],5)."</td><td><img src='images/content/explore2.png' style='height:1.5em;margin-top:-0.3em;cursor:pointer;' onClick=\"LoadValidationHeatMap('$c[1]','$path','$method','Jan2015','$seed_gene')\" /></td></tr>";
}
echo "<tr><td colspan=4 class=last>&nbsp;</td></tr>";
echo "</table></span></li>";
fclose($fh);
// heatmap.
$comm = "ls ../$path/IMAGES/$seed_gene"."_*png";
$outfile = substr(rtrim(`$comm`),3);
if ($outfile != '') {
	echo "<div class='toright' style='width:49%'><img src='$outfile' style='width:100%'/></div>";
}

echo "<br style='clear:both;'/>";



?>
