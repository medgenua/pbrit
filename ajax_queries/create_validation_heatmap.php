<?php
require("../includes/LoadCredentials.php");

if (!isset($_GET['p']) || !file_exists("$scriptdir/".$_GET['p'])) {
	echo "<p>invalid_path</p>";
	exit;
}

if (!isset($_GET['g']) ) {
	echo "<p>no gene provided</p>";
	exit;
}

$job_path = $_GET['p'];
$gene = $_GET['g'];
if (!file_exists("$scriptdir/$job_path/gene_heatmaps")) {
	mkdir("$scriptdir/$job_path/gene_heatmaps");
}
$db_version = $_GET['d'];
$method = $_GET['m'];
$seed = $_GET['s'];
// if heatmap does not exist, create it.
if (!file_exists("$scriptdir/$job_path/gene_heatmaps/$gene.png")) {
	// get db_version
	system("/opt/software/R/3.2.1/bin/Rscript '$scriptdir/Analysis_Files/extractValidationAnnotationMatrix.R' '$gene' '$scriptdir/$job_path/' '$method' '$db_version' '$seed'");
}

if (!file_exists("$scriptdir/$job_path/gene_heatmaps/$gene.png")) {
	echo "ERROR: plot could not be created\n";
}
else {
	echo "$job_path/gene_heatmaps/$gene.png";
}
?>
