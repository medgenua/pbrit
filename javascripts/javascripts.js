function goBack() {
    window.history.back();
}
function GetXmlHttpObject() {
	if (window.XMLHttpRequest) {
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  return new XMLHttpRequest();
	}
	if (window.ActiveXObject) {
  		// code for IE6, IE5
  		return new ActiveXObject("Microsoft.XMLHTTP");
  	}
	return null;
}

function load_loocv() {
	var path = document.getElementById("loocv_path").options[document.getElementById("loocv_path").selectedIndex].value;
	var xmlhttp = GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}
	document.getElementById('loocv_results').innerHTML = "<div style='text-align:center'><img src='images/layout/loading.gif' ><br/>Loading LOOCV details</div>";
	var url="ajax_queries/load_loocv.php";
	url = url+"?p="+path;
	xmlhttp.onreadystatechange=function() {
        	if (xmlhttp.readyState==4) {
                	document.getElementById('loocv_results').innerHTML = xmlhttp.responseText;
        	}
        };
	xmlhttp.open("GET",url,true);
        xmlhttp.send(null);
}

// load gene_by_annotation heatmap on the results page.
function LoadHeatMap(gene,job_path) {
	// set overlay.
	var content = "<div id='plot_area'><div style='border:0.2em dashed #aaa; '><span style='color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em;'>Annotation Source Contributions to "+gene+" Ranking</span>";
	content += "<p style='text-align:center'><img src='images/layout/loader-bar.gif' style='margin-top:3em;margin-bottom:3em;'></p>";
	content += '<p style="text-align:center"><input type=submit value="Close" onClick="document.getElementById(\'mapoverlay\').style.display=\'none\'"></p>';
	content += '</div></div>';
	document.getElementById('mapoverlay').innerHTML = content;
	document.getElementById('mapoverlay').style.display = '';	
	//generate the plot, returns the path.
	var url="ajax_queries/create_heatmap.php";
	url = url+"?g="+gene+"&p="+job_path;
	var xmlhttp = GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}

	xmlhttp.onreadystatechange=function() {
        	if (xmlhttp.readyState==4) {
			var heatmap_path = xmlhttp.responseText;
			var close_button = "<p style='position:absolute;bottom:10;right:20;color:red;font-weight:bold;cursor:pointer' onClick=\"document.getElementById('mapoverlay').style.display='none'\">CLOSE</p>";
			document.getElementById('plot_area').innerHTML = '<span style="position:relative">'+close_button+'<img src="'+heatmap_path+'" style="max-height:100%;max-width:100%;border:0.2em dashed #aaa;padding:1em;"/>'+"</span>";
        	}
        };
	xmlhttp.open("GET",url,true);
        xmlhttp.send(null);


}
// load gene_by_annotation heatmap on the results page.
function LoadValidationHeatMap(gene,job_path,method,db_version,seed) {
	// set overlay.
	var content = "<div id='plot_area'><div style='border:0.2em dashed #aaa; '><span style='color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em;'>Annotation Source Contributions to "+gene+" Ranking</span>";
	content += "<p style='text-align:center'><img src='images/layout/loader-bar.gif' style='margin-top:3em;margin-bottom:3em;'></p>";
	content += '<p style="text-align:center"><input type=submit value="Close" onClick="document.getElementById(\'mapoverlay\').style.display=\'none\'"></p>';
	content += '</div></div>';
	document.getElementById('mapoverlay').innerHTML = content;
	document.getElementById('mapoverlay').style.display = '';	
	//generate the plot, returns the path.
	var url="ajax_queries/create_validation_heatmap.php";
	url = url+"?g="+gene+"&p="+job_path+"&m="+method+"&d="+db_version+"&s="+seed;
	var xmlhttp = GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}

	xmlhttp.onreadystatechange=function() {
        	if (xmlhttp.readyState==4) {
			var heatmap_path = xmlhttp.responseText;
			var close_button = "<p style='position:absolute;bottom:10;right:20;color:red;font-weight:bold;cursor:pointer' onClick=\"document.getElementById('mapoverlay').style.display='none'\">CLOSE</p>";
			document.getElementById('plot_area').innerHTML = '<span style="position:relative">'+close_button+'<img src="'+heatmap_path+'" style="max-height:100%;max-width:100%;border:0.2em dashed #aaa;padding:1em;"/>'+"</span>";
        	}
        };
	xmlhttp.open("GET",url,true);
        xmlhttp.send(null);


}
function LoadModelDetails(job_path) {
	// set overlay.
	var content = "<p><span class=emph>Regression Parameters:</span></p><p>Loading...</p>";
	document.getElementById('regression_details').innerHTML = content;
	//generate the plot, returns the path.
	var url="ajax_queries/load_model_details.php";
	url = url+"?p="+job_path;
	var xmlhttp = GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}

	xmlhttp.onreadystatechange=function() {
        	if (xmlhttp.readyState==4) {
			content = xmlhttp.responseText;
			document.getElementById('regression_details').innerHTML = content;
        	}
        };
	xmlhttp.open("GET",url,true);
        xmlhttp.send(null);


}

function toggle_display(div_name) {
	div_names = ['heatmap', 'ranking_details', 'regression_details'];
	for (var idx in div_names) {
		if (div_name === div_names[idx]) {
			document.getElementById(div_names[idx]).style.display='';
		} else {
			document.getElementById(div_names[idx]).style.display='none';
		}
	}
}

function LoadRankDetails() {
	var test = document.getElementById('browser_testgene').options[document.getElementById('browser_testgene').selectedIndex].value;
	var train = document.getElementById('browser_traingene').options[document.getElementById('browser_traingene').selectedIndex].value;
	var anno = document.getElementById('browser_anno').options[document.getElementById('browser_anno').selectedIndex].value;
	var job_path = document.getElementById('job_path').value;
	var db_version = document.getElementById('db_version').value;
	if (test == '' || train == '' || anno == '') {
		return;
	}
	var xmlhttp = GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}
	document.getElementById('rank_browser_table').innerHTML = "<p>&nbsp;</p><p>Loading Details...</p>";
	var url="ajax_queries/load_ranking_details.php";
	url = url+"?test="+test;
	url = url+"&train="+train;
	url = url+"&anno="+anno;
	url = url+"&path="+job_path;
	url = url+"&dbv="+db_version;
	console.log(url)
	xmlhttp.onreadystatechange=function() {
        	if (xmlhttp.readyState==4) {
                	document.getElementById('rank_browser_table').innerHTML = xmlhttp.responseText;
        	}
        };
	xmlhttp.open("GET",url,true);
        xmlhttp.send(null);
	return;

	
}

